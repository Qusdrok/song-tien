class AppEndpoint {
  AppEndpoint._();

  static const String BASE_URL = "http://kingbuy.vn/";

  static const int connectionTimeout = 15000;
  static const int receiveTimeout = 15000;
  static const String keyAuthorization = "Authorization";

  static const int SUCCESS = 200;
  static const int ERROR_TOKEN = 401;
  static const int ERROR_VALIDATE = 422;
  static const int ERROR_SERVER = 500;
  static const int ERROR_DISCONNECT = -1;

  // Đăng ký
  static const String REGISTER_PHONE = '/registryByEmail';

  // Đăng nhập
  static const String LOGIN_PHONE = "/loginnormal";
  static const String LOGIN_GOOGLE = "/loginbygoogle";
  static const String LOGIN_APPLE = "/loginbyapple";
  static const String LOGIN_FB = "/loginbyfacebook";

  // Quên mật khẩu - reset bằng email
  static const String RESET_PASSWORD_BY_EMAIL = "/resetPasswordByEmail";

  // Tỉnh/thành phố, quận/huyện
  static const String GET_PROVINCE = "/api/getAllProvince";
  static const String GET_DISTRICT = "/api/getDistrictByProvinceCode/03";
}

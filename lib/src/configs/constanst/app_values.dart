class AppValues {
  AppValues._();

  static const double INPUT_FORM_HEIGHT = 55;
  static const double HEIGHT_APP_BAR = 60;
  static const double TWO_PI = 3.14 * 2;
}

import 'package:flutter/material.dart';

import '../../src.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  AccountViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AccountViewModel>(
      viewModel: AccountViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "account",
                  isBack: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.only(top: 12, left: 12, right: 12),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildUser(),
          const SizedBox(height: 8),
          _buildIcon(
            iconData: Icons.shield,
            title: "Điều khoản - chính sách",
            onTap: () => Navigator.pushNamed(context, Routers.privacy_policy),
          ),
          const SizedBox(height: 8),
          _buildIcon(
            iconData: Icons.lock_outlined,
            title: "Thay đổi mật khẩu",
            onTap: () => Navigator.pushNamed(context, Routers.change_password),
          ),
          const SizedBox(height: 8),
          _buildIcon(
            iconData: Icons.close,
            colorIcon: AppColors.red,
            title: "Đăng xuất",
            onTap: () => _viewModel.handleConfirm(msg: "Bạn có chắc chắn đăng xuất tài khoản?"),
          ),
        ],
      ),
    );
  }

  Widget _buildUser() {
    return StreamBuilder(
      stream: AppShared.watchUser(),
      builder: (context, snapshot) {
        bool enabled = !snapshot.hasData;
        var data = snapshot.data;
        return Column(
          children: [
            enabled
                ? WidgetShimmer(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _buildAvatar(null),
                        const SizedBox(height: 20),
                        Text(
                          "Trương Thị Huyền",
                          style: AppStyles.DEFAULT_LARGE_BOLD,
                        ),
                      ],
                    ),
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildAvatar(data),
                      const SizedBox(height: 20),
                      Text(
                        data.name,
                        style: AppStyles.DEFAULT_LARGE_BOLD,
                      ),
                    ],
                  ),
            const SizedBox(height: 35),
            _buildIcon(
              iconData: Icons.account_circle_outlined,
              title: "Thông tin cá nhân",
              onTap: () => data == null ? {} : Navigator.pushNamed(context, Routers.account_update),
            ),
          ],
        );
      },
    );
  }

  Widget _buildAvatar(UserModel user) {
    return Column(
      children: [
        PhysicalModel(
          borderRadius: BorderRadius.all(Radius.circular(130)),
          color: Colors.transparent,
          elevation: 3,
          child: Stack(
            children: [
              Container(
                width: 130,
                height: 130,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.5,
                    color: AppColors.white,
                  ),
                  shape: BoxShape.circle,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(130)),
                  child: WidgetImageNetwork(
                    url: user?.url ?? "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg",
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Positioned(
                bottom: 1,
                right: 5,
                child: GestureDetector(
                  onTap: () => _viewModel.uploadAvatar(user),
                  child: Container(
                    width: 28,
                    height: 28,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1.5,
                        color: AppColors.white,
                      ),
                      shape: BoxShape.circle,
                      color: AppColors.red,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.camera_alt_outlined,
                        color: AppColors.white,
                        size: 15,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildIcon({Function onTap, IconData iconData, String title, Color colorIcon}) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Container(
        width: AppSize.screenWidth,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
        ),
        padding: EdgeInsets.only(left: 12),
        alignment: Alignment.center,
        child: Row(
          children: [
            PhysicalModel(
              borderRadius: BorderRadius.all(Radius.circular(130)),
              color: Colors.transparent,
              elevation: 3,
              child: Container(
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.5,
                    color: AppColors.white,
                  ),
                  shape: BoxShape.circle,
                  color: colorIcon ?? AppColors.grey,
                ),
                child: Center(
                  child: Icon(
                    iconData,
                    color: AppColors.white,
                    size: 15,
                  ),
                ),
              ),
            ),
            const SizedBox(width: 7),
            Text(
              title,
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                fontSize: 16.5,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class AccountViewModel extends BaseViewModel {
  init() {}

  uploadAvatar(UserModel user) async {
    final file = await showDialog(
      context: context,
      builder: (context) => DialogMethodUpload(),
    );

    if (file != null) {
      setLoading(true);
      UploadTask task = await AppFirebase.uploadImageFile(file);
      task.whenComplete(() async {
        String url = await AppFirebase.downloadLink(task.snapshot.ref);
        AppFirebase.userCollection.doc(user.uid).set({'imageUrl': url});

        await AppShared.setUser(new UserModel(
          uid: user.uid,
          url: url,
          type: user.type,
          email: user.email,
          phone: user.phone,
          name: user.name,
        ));
      });
      setLoading(false);
    }
  }

  Future handleConfirm({String msg}) async {
    await showRequestConfirm(
      content: msg,
      transTitle: false,
      actionConfirm: () => signOut(),
    );
  }

  Future<void> signOut() async {
    await AppFirebase.firebaseAuth.signOut();
    Navigator.pushNamed(context, Routers.splash);
  }
}

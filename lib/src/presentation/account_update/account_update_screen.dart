import 'package:flutter/material.dart';

import '../../src.dart';

class AccountUpdateScreen extends StatefulWidget {
  @override
  _AccountUpdateScreenState createState() => _AccountUpdateScreenState();
}

class _AccountUpdateScreenState extends State<AccountUpdateScreen> {
  AccountUpdateViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AccountUpdateViewModel>(
      viewModel: AccountUpdateViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Thông tin cá nhân",
                  trans: false,
                ),
                Expanded(
                  child: StreamBuilder(
                    stream: AppShared.watchUser(),
                    builder: (context, snapshot) {
                      bool enabled = !snapshot.hasData;
                      var data = snapshot.data;

                      if (!enabled) _viewModel.initText(data);
                      return enabled ? WidgetShimmer(child: _buildBody(null, true)) : _buildBody(data, false);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody(UserModel user, bool loading) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(12),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildAvatar(user),
          const SizedBox(height: 20),
          _buildTextField(
            controller: _viewModel.nameController,
            hintText: "Tên của bạn",
            readOnly: loading,
          ),
          const SizedBox(height: 8),
          _buildTextValue(
            content: _viewModel.dateSelected == null
                ? "Sinh nhật"
                : AppUtils.convertDateTime2String(
                    _viewModel.dateSelected,
                    format: "dd/MM/yyyy",
                  ),
            colorContent: _viewModel.dateSelected == null ? AppColors.grey : AppColors.black,
            iconData: Icons.date_range_sharp,
            onTap: () => !loading ? _viewModel.showDate() : {},
          ),
          const SizedBox(height: 8),
          _buildTextValue(
            content: _viewModel.gender ?? "Nam / nữ",
            colorContent: _viewModel.gender == null ? AppColors.grey : AppColors.black,
            iconData: Icons.arrow_drop_down,
            onTap: () => !loading ? _buildBottomSheet() : {},
          ),
          const SizedBox(height: 8),
          _buildTextField(
            controller: _viewModel.emailController,
            hintText: "Email",
            readOnly: loading,
          ),
          const SizedBox(height: 8),
          _buildTextField(
            controller: _viewModel.phoneController,
            hintText: "Số điện thoại",
            readOnly: loading,
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              Expanded(
                child: _buildTextValue(
                  content: _viewModel.city ?? "Tỉnh / thành phố",
                  colorContent: _viewModel.city != null ? AppColors.black : AppColors.grey,
                  iconData: Icons.arrow_drop_down,
                  onTap: () => !loading ? _viewModel.getProvinceAndCity(SearchType.Province) : {},
                ),
              ),
              const SizedBox(width: 8),
              Expanded(
                child: _buildTextValue(
                  content: _viewModel.province ?? "Quận / huyện",
                  colorContent: _viewModel.province != null ? AppColors.black : AppColors.grey,
                  iconData: Icons.arrow_drop_down,
                  onTap: () => !loading ? _viewModel.getProvinceAndCity(SearchType.District) : {},
                ),
              ),
            ],
          ),
          const SizedBox(height: 8),
          _buildTextField(
            controller: _viewModel.addressController,
            hintText: "Địa chỉ",
            readOnly: loading,
          ),
          SizedBox(height: AppSize.screenWidth / 3),
          WidgetButtonGradientAni(
            width: AppSize.screenWidth,
            title: "Cập nhật",
            trans: false,
            colorStart: AppColors.primaryDark,
            colorEnd: AppColors.primaryDark,
            action: () => !loading
                ? _viewModel.handleConfirm(
                    msg: "Bạn chắc chắn\nthay đổi thông tin cá nhân?",
                    actionConfirm: () => _viewModel.uploadInformation(user),
                  )
                : {},
            textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.white,
            ),
          ),
          const SizedBox(height: 8),
        ],
      ),
    );
  }

  Widget _buildAvatar(UserModel user) {
    return Column(
      children: [
        PhysicalModel(
          borderRadius: BorderRadius.all(Radius.circular(130)),
          color: Colors.transparent,
          elevation: 3,
          child: Stack(
            children: [
              Container(
                width: 130,
                height: 130,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.5,
                    color: AppColors.white,
                  ),
                  shape: BoxShape.circle,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(130)),
                  child: WidgetImageNetwork(
                    url: user?.url ?? "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg",
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Positioned(
                bottom: 1,
                right: 5,
                child: GestureDetector(
                  onTap: () => _viewModel.uploadAvatar(user),
                  child: Container(
                    width: 28,
                    height: 28,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1.5,
                        color: AppColors.white,
                      ),
                      shape: BoxShape.circle,
                      color: AppColors.red,
                    ),
                    child: Center(
                      child: Icon(
                        Icons.camera_alt_outlined,
                        color: AppColors.white,
                        size: 15,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Future<Widget> _buildBottomSheet() async {
    _viewModel.gender = await showModalBottomSheet(
      backgroundColor: AppColors.white,
      context: context,
      builder: (c) {
        return Container(
          height: 150,
          padding: EdgeInsets.only(top: 19.5, left: 15, right: 15),
          child: Column(
            children: [
              _buildTextValue(
                content: "Nam",
                iconData: Icons.trending_up,
                colorIcon: _viewModel.gender == "Nam" ? AppColors.primaryDark : Colors.transparent,
                colorContent: AppColors.black,
                onTap: () => _viewModel.setGender("Nam"),
              ),
              const SizedBox(height: 12),
              _buildTextValue(
                content: "Nữ",
                iconData: Icons.trending_up,
                colorIcon: _viewModel.gender == "Nữ" ? AppColors.primaryDark : Colors.transparent,
                colorContent: AppColors.black,
                onTap: () => _viewModel.setGender("Nữ"),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildTextValue({
    String content,
    IconData iconData,
    Color colorContent,
    Color colorIcon,
    Function onTap,
  }) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Container(
        width: AppSize.screenWidth,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Colors.white,
        ),
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: 12, right: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                content,
                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: colorContent ?? AppColors.grey,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Icon(
              iconData,
              size: 28,
              color: colorIcon ?? AppColors.red,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextField({
    TextEditingController controller,
    String hintText,
    bool readOnly = false,
  }) {
    return TextFormField(
      controller: controller,
      readOnly: readOnly,
      decoration: InputDecoration(
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(Colors.transparent),
        contentPadding: EdgeInsets.fromLTRB(15.0, 12.0, 15.0, 12.0),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../../src.dart';

class AccountUpdateViewModel extends BaseViewModel {
  TextEditingController nameController = TextEditingController();
  TextEditingController birthDayController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  DateTime dateSelected;
  String gender;
  String city;
  String province;

  init() {}

  initText(UserModel user) {
    nameController.text = user.name;
    emailController.text = user.email;
    phoneController.text = user.phone;
  }

  uploadInformation(UserModel user) async {
    setLoading(true);
    try {
      UserModel _user = new UserModel(
        uid: user.uid,
        url: user.url,
        type: user.type,
        name: nameController.text.trim(),
        phone: phoneController.text.trim(),
        email: emailController.text.trim(),
      );

      Navigator.pop(context);
      await AppShared.setUser(_user);

      AppFirebase.userCollection.doc(user.uid).set(_user.toJson());
      Toast.show("Update thông tin thành công", context);
      Navigator.pop(context);
      setLoading(false);
    } catch (e) {
      handleConfirm(msg: e.toString());
      setLoading(false);
    }
  }

  uploadAvatar(UserModel user) async {
    final file = await showDialog(
      context: context,
      builder: (context) => DialogMethodUpload(),
    );

    if (file != null) {
      setLoading(true);
      UploadTask task = await AppFirebase.uploadImageFile(file);
      task.whenComplete(() async {
        String url = await AppFirebase.downloadLink(task.snapshot.ref);
        AppFirebase.userCollection.doc(user.uid).set({'imageUrl': url});

        await AppShared.setUser(new UserModel(
          uid: user.uid,
          url: url,
          type: user.type,
          email: user.email,
          phone: user.phone,
          name: user.name,
        ));
      });
      setLoading(false);
    }
  }

  getProvinceAndCity(SearchType searchType) async {
    switch (searchType) {
      case SearchType.District:
        province = await Navigator.pushNamed(
          context,
          Routers.search,
          arguments: SearchType.District,
        ) as String;
        break;
      case SearchType.Province:
        city = await Navigator.pushNamed(
          context,
          Routers.search,
          arguments: SearchType.Province,
        ) as String;
        break;
    }

    notifyListeners();
  }

  Future<void> setGender(String gender) async {
    Navigator.pop(context, gender);
    await Future.delayed(Duration(milliseconds: 250));
    unFocus();
    notifyListeners();
  }

  Future handleConfirm({String msg, Function actionConfirm}) async {
    await showRequestConfirm(
      content: msg,
      transTitle: false,
      actionConfirm: actionConfirm,
    );
  }

  Future<void> showDate() async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: dateSelected ?? DateTime.now(),
      firstDate: DateTime(1990),
      lastDate: DateTime(9999),
    );

    if (picked != null && picked != dateSelected) {
      dateSelected = picked;
      notifyListeners();
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';

import '../../src.dart';

abstract class BaseViewModel extends ChangeNotifier {
  final AuthRepository authRepository = AuthRepository();

  final loadingSubject = BehaviorSubject<bool>();
  final errorSubject = BehaviorSubject<String>();

  BuildContext _context;

  BuildContext get context => _context;

  setContext(BuildContext value) => _context = value;

  void setLoading(bool loading) {
    if (loading != isLoading) loadingSubject.add(loading);
  }

  bool get isLoading => loadingSubject.value;

  void setError(String message) => errorSubject.add(message);

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  @override
  void dispose() async {
    await loadingSubject.drain();
    loadingSubject.close();
    await errorSubject.drain();
    errorSubject.close();
    super.dispose();
  }

  Future<dynamic> requestLogin() async {
    await showDialog(
      context: context,
      builder: (context) => DialogConfirm(
        keyTitle: "notification",
        actionConfirm: () {
          //Navigator.popAndPushNamed(context, Routers.login);
        },
        content: "request_login",
        transContent: true,
      ),
    );
  }

  void unFocus() {
    FocusScope.of(context).unfocus();
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  Future<bool> showRequestConfirm({
    @required String content,
    String keyTitle,
    bool transTitle = true,
    bool transContent = false,
    Function actionCancel,
    Function actionConfirm,
  }) async {
    return await showDialog(
      context: context,
      builder: (context) => DialogConfirm(
        keyTitle: keyTitle,
        actionCancel: actionCancel,
        transTitle: transTitle,
        transContent: transContent,
        content: content,
        actionConfirm: actionConfirm,
      ),
    );
  }

  Future<dynamic> showNotification({
    @required String keyTitle,
    bool trans = true,
    String keyAction,
    Function action,
  }) async {
    await showDialog(
      context: context,
      builder: (context) => DialogError(
        keyTitle: keyTitle,
        action: action,
        trans: trans,
        keyAction: keyAction,
      ),
    );
  }
}

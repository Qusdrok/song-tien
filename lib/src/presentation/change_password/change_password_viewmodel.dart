import 'package:flutter/material.dart';

import '../../src.dart';

class ChangePasswordViewModel extends BaseViewModel {
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmNewPasswordController = TextEditingController();
  GlobalKey<FormState> form = GlobalKey<FormState>();

  bool obSecureText = true;

  init() {}

  showPassword() {
    obSecureText = !obSecureText;
    notifyListeners();
  }

  Future handleConfirm({String msg}) async {
    await showRequestConfirm(
      content: msg,
      transTitle: false,
    );
  }
}

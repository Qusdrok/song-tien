import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../../src.dart';

class CustomerInfoScreen extends StatefulWidget {
  @override
  _CustomerInfoScreenState createState() => _CustomerInfoScreenState();
}

class _CustomerInfoScreenState extends State<CustomerInfoScreen> {
  CustomerInfoViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CustomerInfoViewModel>(
      viewModel: CustomerInfoViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SingleChildScrollView(
            child: SafeArea(
              child: Column(
                children: [
                  WidgetAppBar(
                    keyTitle: "Thông Tin Hành Khách",
                    trans: false,
                    height: AppSize.screenWidth / 1.5,
                    children: Container(
                      height: AppSize.screenWidth / 1.5,
                      child: Stack(
                        children: [
                          Center(
                            child: QrImage(
                              data: "1234567890",
                              version: QrVersions.auto,
                              size: 170,
                              foregroundColor: Colors.white,
                              backgroundColor: Colors.transparent,
                            ),
                          ),
                          Container(
                            child: Center(
                              child: CustomPaint(
                                painter: QRBorderPainter(),
                                child: Container(
                                  width: BarReaderSize.width,
                                  height: BarReaderSize.height,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  _buildBody(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildHeader("Mã xác nhận đặt chỗ: ABC0123"),
          Divider(
            thickness: 2,
            color: AppColors.primaryLight,
          ),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Mã chuyến", content: "C001"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Mã chỗ ngồi", content: "A1"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Khách hàng", content: "Trần Phương Anh"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Số điện thoại", content: "036411999"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Người liên hệ", content: "Nguyễn Hằng"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Người đặt", content: "Huỳnh Gia"),
          const SizedBox(height: 7),
          _buildRowInfo(
            header: "Thời gian đặt",
            content: AppUtils.convertDateTime2String(
              DateTime.now(),
              format: "hh:MM - dd/MM/yyyy",
            ),
          ),
          const SizedBox(height: 7),
          _buildRowInfo(
            header: "Trạng thái",
            content: "Chờ xác nhận",
            colorContent: AppColors.red,
          ),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Người duyệt", content: "Trần Duyệt"),
          const SizedBox(height: 7),
          _buildRowInfo(
            header: "Thời gian duyệt",
            content: AppUtils.convertDateTime2String(
              DateTime.now(),
              format: "hh:MM - dd/MM/yyyy",
            ),
          ),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Ghi chú", content: "---"),
          const SizedBox(height: 15),
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, Routers.customer_info_edit),
            child: Text(
              "Chỉnh sửa thông tin khách hàng",
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: AppColors.primaryDark,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
          const SizedBox(height: 30),
          WidgetButtonGradientAni(
            width: AppSize.screenWidth,
            title: "Huỷ khách hàng",
            trans: false,
            colorStart: AppColors.primaryDark,
            colorEnd: AppColors.primaryDark,
            action: () {},
            textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader(String header) {
    return Text(
      header,
      style: AppStyles.DEFAULT_MEDIUM,
    );
  }

  Widget _buildRowInfo({String header, String content, Color colorContent}) {
    return Row(
      children: [
        Expanded(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              header,
              style: AppStyles.DEFAULT_MEDIUM,
            ),
          ),
        ),
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              content,
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: colorContent ?? AppColors.black,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ],
    );
  }
}

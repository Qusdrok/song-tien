import 'package:flutter/material.dart';

import '../../src.dart';

class CustomerInfoEditScreen extends StatefulWidget {
  @override
  _CustomerInfoEditScreenState createState() => _CustomerInfoEditScreenState();
}

class _CustomerInfoEditScreenState extends State<CustomerInfoEditScreen> {
  CustomerInfoEditViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CustomerInfoEditViewModel>(
      viewModel: CustomerInfoEditViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Chỉnh Sửa Thông Tin Khách Hàng",
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildHeader("Thông tin đặt chỗ"),
          const SizedBox(height: 15),
          _buildRowInfo(header: "Mã chuyến", content: "C001"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Người liên hệ", content: "Nguyễn Hằng"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Người đặt", content: "Nguyễn Hằng"),
          const SizedBox(height: 15),
          _buildHeader("Thông tin hành khách"),
          const SizedBox(height: 15),
          _buildRowInfo(header: "Mã chỗ ngồi", content: "A1"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Tên khách hàng", content: ""),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Số điện thoại", content: ""),
          const SizedBox(height: 7),
          Container(
            height: 85,
            width: AppSize.screenWidth,
            child: _buildTextField(hintText: "Ghi chú"),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader(String header) {
    return Text(
      header,
      style: AppStyles.DEFAULT_MEDIUM_BOLD,
    );
  }

  Widget _buildRowInfo({String header, String content, Widget child}) {
    return Container(
      width: AppSize.screenWidth,
      height: 50,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Expanded(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                header,
                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: AppColors.grey,
                ),
              ),
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: child ??
                  Text(
                    content,
                    style: AppStyles.DEFAULT_MEDIUM,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTextField({String hintText}) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.top,
      expands: true,
      maxLines: null,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        contentPadding: EdgeInsets.all(10),
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(Colors.transparent),
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

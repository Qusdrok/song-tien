import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';

import '../../src.dart';

class EnterOTPScreen extends StatefulWidget {
  final String phoneNumber;

  EnterOTPScreen({this.phoneNumber});

  @override
  _EnterOTPScreenState createState() => _EnterOTPScreenState();
}

class _EnterOTPScreenState extends State<EnterOTPScreen> {
  EnterOTPViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<EnterOTPViewModel>(
      viewModel: EnterOTPViewModel(phoneNumber: widget.phoneNumber),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                _buildHeader(),
                _buildBody(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(60),
          ),
          child: Image.asset(
            AppImages.imgBgLogin,
            width: AppSize.screenWidth,
            height: 210,
            fit: BoxFit.fill,
          ),
        ),
        const SizedBox(height: 10),
        WidgetLogo(),
      ],
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: GestureDetector(
        onTap: _viewModel.unFocus,
        child: StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool loading = snapshot.data ?? false;
            return Container(
              width: AppSize.screenWidth,
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              color: Colors.transparent,
              child: _buildEnterOTP(loading),
            );
          },
        ),
      ),
    );
  }

  Widget _buildEnterOTP(bool loading) {
    return Column(
      children: [
        Container(
          width: AppSize.screenWidth,
          height: 110,
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 10),
          child: Text(
            AppLocalizations.of(context).translate("otp").toUpperCase(),
            style: AppStyles.DEFAULT_LARGE.copyWith(
              color: AppColors.primary,
              fontSize: 24,
            ),
          ),
        ),
        Text(
          AppLocalizations.of(context).translate("otp_request"),
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.black,
            fontSize: 17,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 15),
        _buildPin(),
        SizedBox(height: 15),
        WidgetButtonGradientAni(
          width: AppSize.screenWidth - 15,
          title: "continue",
          colorStart: AppColors.primaryDark,
          colorEnd: AppColors.primaryDark,
          action: () {},
          loading: loading,
          textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.white,
          ),
        ),
      ],
    );
  }

  Widget _buildPin() {
    final BoxDecoration pinPutDecoration = BoxDecoration(
      border: Border.all(color: AppColors.primaryLight),
      borderRadius: BorderRadius.circular(5.0),
    );

    return PinPut(
      fieldsCount: 6,
      eachFieldHeight: 40,
      withCursor: true,
      controller: _viewModel.pinController,
      onSubmit: _viewModel.signInPhoneNumber,
      submittedFieldDecoration: pinPutDecoration,
      selectedFieldDecoration: pinPutDecoration,
      followingFieldDecoration: pinPutDecoration,
    );
  }
}

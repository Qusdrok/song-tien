import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class EnterOTPViewModel extends BaseViewModel {
  TextEditingController pinController = TextEditingController();
  String verificationID;
  String phoneNumber;

  EnterOTPViewModel({this.phoneNumber});

  init() => verifyPhoneNumber();

  Future<void> signInPhoneNumber(String pin) async {
    try {
      setLoading(true);
      await AppFirebase.firebaseAuth
          .signInWithCredential(
        PhoneAuthProvider.credential(
          verificationId: this.verificationID,
          smsCode: pin,
        ),
      )
          .then((value) async {
        if (value.user != null) {
          setLoading(false);
          await handleFailure(msg: "Đăng ký thành công");
          Navigator.pushReplacementNamed(context, Routers.navigation);
        } else {
          setLoading(false);
        }
      });
    } catch (e) {
      pinController.clear();
      unFocus();
      setLoading(false);
      await handleFailure(msg: "Mã xác minh bạn nhập vào không đúng");
    }
  }

  void verifyPhoneNumber() async {
    await AppFirebase.firebaseAuth.verifyPhoneNumber(
      phoneNumber: convertPhoneNumber(phoneNumber.trim()),
      verificationCompleted: (credential) {
        print("Đăng ký thành công");
      },
      timeout: const Duration(seconds: 60),
      verificationFailed: (e) async {
        setLoading(false);
        await handleFailure(msg: "Xác minh thất bại");
      },
      codeSent: (verificationID, resendToken) async {
        this.verificationID = verificationID;
      },
      codeAutoRetrievalTimeout: (verificationID) async {
        setLoading(false);
        await handleFailure(msg: "Code đã hết hạn sử dụng");
      },
    );
  }

  String convertPhoneNumber(String phoneNumber) {
    if (phoneNumber.startsWith("+84")) return phoneNumber;
    return phoneNumber.replaceFirst("0", "+84");
  }

  Future handleFailure({String msg}) async {
    await showNotification(
      keyTitle: msg ?? "Lỗi",
      trans: false,
    );
  }
}

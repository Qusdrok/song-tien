import 'package:flutter/material.dart';

import '../../src.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  ForgotPasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ForgotPasswordViewModel>(
      viewModel: ForgotPasswordViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                _buildHeader(),
                _buildBody(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildHeader() {
    return Stack(
      children: [
        Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(60),
              ),
              child: Image.asset(
                AppImages.imgBgLogin,
                width: AppSize.screenWidth,
                height: 210,
                fit: BoxFit.fill,
              ),
            ),
            const SizedBox(height: 10),
            WidgetLogo(),
          ],
        ),
        WidgetButtonBack(
          color: Colors.white,
          size: 18,
        ),
      ],
    );
  }

  Widget _buildBody() {
    return Container(
      width: AppSize.screenWidth,
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
      color: Colors.transparent,
      child: _buildForgotPassword(),
    );
  }

  Widget _buildForgotPassword() {
    return Column(
      children: [
        Container(
          width: AppSize.screenWidth,
          height: 110,
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 10),
          child: Text(
            AppLocalizations.of(context).translate("forgot_password").toUpperCase(),
            style: AppStyles.DEFAULT_LARGE.copyWith(
              color: AppColors.primary,
              fontSize: 24,
            ),
          ),
        ),
        Form(
          key: _viewModel.formForgotPassword,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              _buildTextField(
                validator: AppValid.validatePhoneNumber(context),
                controller: _viewModel.phoneController,
                hintTranslateText: "phone",
              ),
              SizedBox(height: 15),
              WidgetButtonGradientAni(
                width: AppSize.screenWidth - 15,
                title: "continue",
                colorStart: AppColors.primaryDark,
                colorEnd: AppColors.primaryDark,
                action: () => _viewModel.switchScreen(),
                textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: AppColors.white,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildTextField({
    TextEditingController controller,
    String hintTranslateText,
    TextInputType inputType,
    validator,
  }) {
    return TextFormField(
      controller: controller,
      validator: validator,
      keyboardType: inputType ?? TextInputType.text,
      decoration: InputDecoration(
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(AppColors.primaryLight),
        errorBorder: _buildBorderTextField(AppColors.red),
        focusedErrorBorder: _buildBorderTextField(AppColors.primaryLight),
        contentPadding: EdgeInsets.fromLTRB(25.0, 16.0, 25.0, 16.0),
        filled: true,
        fillColor: Colors.white,
        hintText: AppLocalizations.of(context).translate(hintTranslateText),
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(150),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';

import '../../src.dart';

class ForgotPasswordViewModel extends BaseViewModel {
  TextEditingController phoneController = TextEditingController();
  GlobalKey<FormState> formForgotPassword = GlobalKey<FormState>();

  init() {}

  switchScreen() {
    unFocus();
    if (!formForgotPassword.currentState.validate()) return;
    Navigator.pushNamed(context, Routers.enter_otp, arguments: phoneController.text);
  }
}

import 'dart:ui';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../../src.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeViewModel>(
      viewModel: HomeViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: _buildBody(),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Stack(
        children: [
          DottedBorder(
            borderType: BorderType.RRect,
            radiusBottomLeft: Radius.circular(60),
            radiusBottomRight: Radius.circular(60),
            dashPattern: [8, 5],
            strokeWidth: 1.5,
            color: AppColors.primaryDark,
            child: Container(
              width: AppSize.screenWidth,
              height: 200,
              decoration: BoxDecoration(
                color: AppColors.primaryLight,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(60),
                  bottomRight: Radius.circular(60),
                ),
              ),
            ),
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 30),
                child: Row(
                  children: [
                    Expanded(child: WidgetLogo()),
                    _buildIconNotification(),
                  ],
                ),
              ),
              const SizedBox(height: 15),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: WidgetCarousel(
                  circular: 8,
                  heightImage: 200,
                  bottomPaddingDot: 0,
                  images: [
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                  ],
                  onPageChanged: _viewModel.setSelected,
                ),
              ),
              const SizedBox(height: 15),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: _buildTitle(translateText: "sightseeing_program"),
              ),
              const SizedBox(height: 10),
              Container(
                width: AppSize.screenWidth,
                height: 235,
                child: ListView.separated(
                  itemCount: 5,
                  physics: BouncingScrollPhysics(),
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  itemBuilder: (context, index) => _buildTour(),
                  separatorBuilder: (context, index) => Container(width: 10),
                  scrollDirection: Axis.horizontal,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15),
                child: WidgetCarousel(
                  circular: 0,
                  heightImage: 200,
                  bottomPaddingDot: 0,
                  images: [
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                    AppImages.imgBgLogin,
                  ],
                  onPageChanged: _viewModel.setSelected,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: _buildTitle(translateText: "news"),
              ),
              const SizedBox(height: 10),
              Container(
                width: AppSize.screenWidth,
                height: 270,
                child: ListView.separated(
                  itemCount: 5,
                  physics: BouncingScrollPhysics(),
                  padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                  itemBuilder: (context, index) => _buildNews(),
                  separatorBuilder: (context, index) => Container(width: 10),
                  scrollDirection: Axis.horizontal,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildTitle({String translateText}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Text(
            AppLocalizations.of(context).translate(translateText),
            style: AppStyles.DEFAULT_LARGE.copyWith(color: AppColors.red),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        Text(
          AppLocalizations.of(context).translate("see_all"),
          style: AppStyles.DEFAULT_SMALL.copyWith(
            color: AppColors.primaryDark,
            fontSize: 13,
          ),
        ),
      ],
    );
  }

  Widget _buildIconNotification() {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, Routers.notification),
      child: CircleAvatar(
        radius: 20,
        backgroundColor: AppColors.white,
        child: Icon(
          Icons.notifications_none_sharp,
          size: 30,
          color: AppColors.primaryDark,
        ),
      ),
    );
  }

  Widget _buildTour() {
    return Stack(
      children: [
        PhysicalModel(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: AppColors.white,
          elevation: 6,
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            child: Image.asset(
              AppImages.imgBg2,
              width: 250,
              height: 200,
              fit: BoxFit.fill,
            ),
          ),
        ),
        Positioned(
          bottom: 2,
          left: 10,
          right: 10,
          child: PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Colors.white,
            elevation: 1,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Tham quan đảo Angel Island bằng du thuyền",
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 5),
                  _buildStar(5),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildStar(int star) {
    return Wrap(
      spacing: 7,
      children: List.generate(
        star,
        (index) => Icon(
          Icons.star,
          size: 12,
          color: AppColors.orangeLight,
        ),
      ),
    );
  }

  Widget _buildNews() {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, Routers.news),
      child: Stack(
        children: [
          PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: AppColors.greyLight,
            elevation: 5,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              child: Image.asset(
                AppImages.imgBg2,
                width: 280,
                height: 280,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: 280,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                color: AppColors.white,
              ),
              padding: EdgeInsets.only(top: 8, left: 8, bottom: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Biệt thự đảo - Xu hướng mới trên thị trường bất động sản siêu sang.",
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                      fontSize: 16,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    "23/1/2021",
                    style: AppStyles.DEFAULT_SMALL.copyWith(
                      color: AppColors.grey,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

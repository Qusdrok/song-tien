import '../../src.dart';

class HomeViewModel extends BaseViewModel {
  int tabSelected = 0;

  init() {}

  setSelected(int index) {
    tabSelected = index;
    notifyListeners();
  }
}

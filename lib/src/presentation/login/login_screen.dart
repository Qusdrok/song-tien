import 'package:flutter/material.dart';

import '../../src.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginViewModel>(
      viewModel: LoginViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                _buildHeader(),
                _buildBody(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(60),
          ),
          child: Image.asset(
            AppImages.imgBgLogin,
            width: AppSize.screenWidth,
            height: 210,
            fit: BoxFit.fill,
          ),
        ),
        const SizedBox(height: 10),
        WidgetLogo(),
      ],
    );
  }

  Widget _buildBody() {
    return AnimatedSwitcher(
      transitionBuilder: (child, animation) => RotationTransition(turns: animation, child: child),
      layoutBuilder: (widget, list) => Stack(children: [widget, ...list]),
      switchInCurve: Curves.easeInBack,
      switchOutCurve: Curves.easeInBack.flipped,
      duration: Duration(seconds: 1),
      child: GestureDetector(
        onTap: _viewModel.unFocus,
        child: StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool loading = snapshot.data ?? false;
            return Container(
              width: AppSize.screenWidth,
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              color: Colors.transparent,
              child: _viewModel.isSignIn ? _buildSignIn(loading) : _buildSignUp(loading),
            );
          },
        ),
      ),
    );
  }

  Widget _buildSignIn(bool loading) {
    return Column(
      children: [
        Container(
          width: AppSize.screenWidth,
          height: 110,
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 10),
          child: Text(
            AppLocalizations.of(context).translate("login").toUpperCase(),
            style: AppStyles.DEFAULT_LARGE.copyWith(
              color: AppColors.primary,
              fontSize: 24,
            ),
          ),
        ),
        Form(
          key: _viewModel.formSignIn,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              _buildTextField(
                validator: AppValid.validatePhoneNumber(context),
                readOnly: loading,
                controller: _viewModel.phoneController,
                hintText: "phone",
                inputType: TextInputType.emailAddress,
              ),
              SizedBox(height: 8),
              _buildTextField(
                validator: AppValid.validatePassword(context),
                readOnly: loading,
                controller: _viewModel.passwordController,
                hintText: "password",
                isPassword: true,
              ),
              SizedBox(height: 10),
              GestureDetector(
                onTap: () => Navigator.pushNamed(context, Routers.forgot_password),
                child: Text(
                  "${AppLocalizations.of(context).translate("forgot_password")} ?",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.black,
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 25),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            WidgetButtonGradientAni(
              width: AppSize.screenWidth,
              title: "login",
              colorStart: AppColors.primaryDark,
              colorEnd: AppColors.primaryDark,
              action: _viewModel.signIn,
              loading: loading,
              textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: AppColors.white,
              ),
            ),
            SizedBox(height: AppSize.screenWidth / 5),
            _buildSocialButtons(loading),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  AppLocalizations.of(context).translate("no_account"),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.black),
                ),
                const SizedBox(width: 5),
                GestureDetector(
                  onTap: !loading ? () => _viewModel.switchScreen() : null,
                  child: Text(
                    AppLocalizations.of(context).translate("register"),
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: AppColors.red,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildSignUp(bool loading) {
    return Column(
      children: [
        Container(
          width: AppSize.screenWidth,
          height: 110,
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 10),
          child: Text(
            AppLocalizations.of(context).translate("register").toUpperCase(),
            style: AppStyles.DEFAULT_LARGE.copyWith(
              color: AppColors.primary,
              fontSize: 24,
            ),
          ),
        ),
        Form(
          key: _viewModel.formSignUp,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildTextField(
                  validator: AppValid.validatePhoneNumber(context),
                  readOnly: loading,
                  controller: _viewModel.phoneController,
                  hintText: "phone",
                ),
                SizedBox(height: 8),
                _buildTextField(
                  validator: AppValid.validatePassword(context),
                  readOnly: loading,
                  controller: _viewModel.passwordController,
                  hintText: "password",
                  isPassword: true,
                ),
                SizedBox(height: 8),
                _buildTextField(
                  validator: AppValid.validatePassword(context),
                  readOnly: loading,
                  controller: _viewModel.confirmPasswordController,
                  hintText: "confirm_password",
                  isPassword: true,
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 25),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            WidgetButtonGradientAni(
              width: AppSize.screenWidth - 15,
              title: "register",
              colorStart: AppColors.primaryDark,
              colorEnd: AppColors.primaryDark,
              action: _viewModel.signUp,
              loading: loading,
              textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: AppColors.white,
              ),
            ),
            SizedBox(height: AppSize.screenWidth / 4),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  AppLocalizations.of(context).translate("have_account"),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.black,
                  ),
                ),
                const SizedBox(width: 5),
                GestureDetector(
                  onTap: () => _viewModel.switchScreen(),
                  child: Text(
                    AppLocalizations.of(context).translate("login"),
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: AppColors.red,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildSocialButtons(bool loading) {
    return Column(
      children: [
        Text(
          AppLocalizations.of(context).translate("or_with"),
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.black,
          ),
        ),
        const SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildIconLogin(
              AppImages.icGoogle,
              () => _viewModel.loginGoogleFirebase(),
              loading,
            ),
            const SizedBox(width: 12),
            _buildIconLogin(
              AppImages.icFacebook,
              () => _viewModel.loginFacebookFirebase(),
              loading,
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildIconLogin(String imageUrl, Function onTap, bool loading) {
    return InkWell(
      onTap: loading ? null : onTap,
      child: Container(
        width: 48,
        height: 48,
        child: Image.asset(
          imageUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildTextField({
    TextEditingController controller,
    String hintText,
    TextInputType inputType,
    validator,
    bool isPassword = false,
    bool readOnly = false,
  }) {
    return TextFormField(
      controller: controller,
      readOnly: readOnly,
      obscureText: isPassword ? _viewModel.obSecureText : false,
      validator: validator,
      keyboardType: inputType ?? TextInputType.text,
      decoration: InputDecoration(
        suffixIcon: isPassword
            ? Padding(
                padding: const EdgeInsets.only(right: 12),
                child: IconButton(
                  icon: Icon(
                    _viewModel.obSecureText ? Icons.remove_red_eye_outlined : Icons.remove_red_eye,
                    color: AppColors.grey,
                  ),
                  color: Colors.black,
                  onPressed: () => _viewModel.showPassword(),
                ),
              )
            : null,
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(AppColors.primaryLight),
        errorBorder: _buildBorderTextField(AppColors.red),
        focusedErrorBorder: _buildBorderTextField(AppColors.primaryLight),
        contentPadding: EdgeInsets.fromLTRB(25.0, 16.0, 25.0, 16.0),
        filled: true,
        fillColor: Colors.white,
        hintText: AppLocalizations.of(context).translate(hintText),
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(150),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:toast/toast.dart';

import '../../src.dart';

class LoginViewModel extends BaseViewModel {
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  GlobalKey<FormState> formSignIn = GlobalKey<FormState>();
  GlobalKey<FormState> formSignUp = GlobalKey<FormState>();

  bool obSecureText = true;
  bool isSignIn = true;

  init() {}

  switchScreen() {
    obSecureText = true;
    isSignIn = !isSignIn;
    phoneController.clear();
    passwordController.clear();
    confirmPasswordController.clear();
    notifyListeners();
  }

  showPassword() {
    obSecureText = !obSecureText;
    notifyListeners();
  }

  signUp() {
    unFocus();
    if (!formSignUp.currentState.validate()) return;
    handleRegister(
      phone: this.phoneController.text,
      password: this.passwordController.text,
    );
  }

  signIn() {
    unFocus();
    if (!formSignIn.currentState.validate()) return;
    Navigator.pushNamedAndRemoveUntil(context, Routers.enter_otp, (route) => false);
  }

  Future<void> handleRegister({
    String phone,
    String password,
  }) async {
    loadingSubject.add(true);
    Navigator.pushNamed(
      context,
      Routers.enter_otp,
      arguments: phoneController.text,
    );
    loadingSubject.add(false);
  }

  void handleLogin({
    String accessToken,
    LoginType type,
    String avatar,
    String email,
    String fullName,
    String password,
    String id,
    String phone,
  }) async {
    try {
      setLoading(true);
      UserModel user = new UserModel(
        url: avatar,
        email: email,
        name: fullName,
        phone: phone,
        uid: id,
        type: Random.secure().nextInt(2),
      );

      switch (type) {
        case LoginType.facebook:
        case LoginType.google:
          final DocumentReference docUser = await AppFirebase.userCollection.doc(id);
          docUser.get().then(
            (data) {
              if (!data.exists) {
                print("Tài khoản chưa thêm vào database");
                AppFirebase.userCollection.doc(id).set(user.toJson());
              } else {
                print("Tài khoản đã thêm vào database");
              }
            },
          );
          break;
        case LoginType.apple:
          break;
        case LoginType.phone:
          break;
      }

      await AppShared.setUser(user);
      Toast.show(AppLocalizations.of(context).translate("login_success"), context);
      Navigator.pushNamedAndRemoveUntil(context, Routers.navigation, (route) => false);
    } catch (e) {
      await handleFailure();
      setLoading(false);
    }
  }

  Future handleFailure({String msg}) async {
    await showNotification(
      keyTitle: msg ?? "login_failure",
      trans: msg == null,
    );
  }

  void loginGoogleFirebase() async {
    try {
      setLoading(true);
      GoogleSignInAccount account = await GoogleSignIn(scopes: ['email']).signIn();
      GoogleSignInAuthentication authentication = await account.authentication;

      AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: authentication.accessToken,
        idToken: authentication.idToken,
      );

      UserCredential authResult = await AppFirebase.firebaseAuth.signInWithCredential(credential);
      User user = authResult.user;

      if (user != null) {
        final User currentUser = AppFirebase.firebaseAuth.currentUser;
        handleLogin(
          avatar: currentUser.photoURL,
          email: currentUser.email,
          type: LoginType.google,
          id: currentUser.uid,
          accessToken: authentication.accessToken,
          fullName: currentUser.displayName,
          phone: currentUser.phoneNumber,
        );
      } else {
        setLoading(false);
      }
    } catch (error) {
      print(error.toString());
      await handleFailure(msg: error.toString());
      setLoading(false);
    }
  }

  void loginFacebookFirebase() async {
    setLoading(true);
    final facebookLogin = FacebookLogin();
    if (await facebookLogin.isLoggedIn) await facebookLogin.logOut();

    final result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final credential = FacebookAuthProvider.credential(result.accessToken.token);
        final user = (await AppFirebase.firebaseAuth.signInWithCredential(credential)).user;

        handleLogin(
          id: result.accessToken.userId,
          type: LoginType.facebook,
          accessToken: result.accessToken.token,
          avatar: user.photoURL,
          email: user.email,
          fullName: user.displayName,
          phone: user.phoneNumber,
        );
        break;
      case FacebookLoginStatus.cancelledByUser:
        await handleFailure(msg: result.errorMessage);
        setLoading(false);
        break;
      case FacebookLoginStatus.error:
        print(result.errorMessage);
        await handleFailure(msg: result.errorMessage);
        setLoading(false);
        break;
    }
  }

  @override
  void dispose() async {
    phoneController.dispose();
    passwordController.dispose();
    super.dispose();
  }
}

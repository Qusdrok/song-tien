import 'package:flutter/material.dart';

import '../../src.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  NavigationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NavigationViewModel>(
      viewModel: NavigationViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return WidgetFadeTransitions(
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: [
                Expanded(child: _buildPages()),
                _buildBottomNavBar(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildButton({int index, IconData iconData, String translateText}) {
    return Expanded(
      child: InkWell(
        onTap: () => _viewModel.switchPage(index),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              iconData,
              size: 30,
              color: _viewModel.currentPage == index ? AppColors.white : AppColors.grey,
            ),
            Text(
              AppLocalizations.of(context).translate(translateText),
              style: AppStyles.DEFAULT_SMALL_BOLD.copyWith(
                color: _viewModel.currentPage == index ? AppColors.white2 : AppColors.grey,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildBottomNavBar() {
    return Container(
      width: AppSize.screenWidth,
      color: AppColors.primaryDark,
      padding: EdgeInsets.only(top: 12, bottom: 12),
      child: Row(
        children: [
          _buildButton(
            index: 0,
            iconData: Icons.home,
            translateText: "home",
          ),
          _buildButton(
            index: 1,
            iconData: Icons.home,
            translateText: "tour",
          ),
          _buildButton(
            index: 2,
            iconData: Icons.home,
            translateText: "contact",
          ),
          _buildButton(
            index: 3,
            iconData: Icons.home,
            translateText: "account",
          ),
        ],
      ),
    );
  }

  Widget _buildPages() {
    switch (_viewModel.currentPage) {
      case 0:
        return HomeScreen();
      case 1:
        return TourScreen();
      case 2:
        return ContactScreen();
      case 3:
        return AccountScreen();
      default:
        return SizedBox();
    }
  }
}

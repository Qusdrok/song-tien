import '../../src.dart';

class NavigationViewModel extends BaseViewModel {
  int currentPage = 0;

  init() async {}

  switchPage(int index) {
    currentPage = index;
    notifyListeners();
  }
}

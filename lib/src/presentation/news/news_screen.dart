import 'package:flutter/material.dart';

import '../../src.dart';

class NewsScreen extends StatefulWidget {
  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  NewsViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NewsViewModel>(
      viewModel: NewsViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  colorButton: AppColors.white,
                  colorTitle: AppColors.white,
                  keyTitle: "news",
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        _buildTabBar(),
        const SizedBox(height: 5),
        Expanded(child: _buildItems()),
      ],
    );
  }

  Widget _buildItems() {
    return ListView.separated(
      separatorBuilder: (context, index) => Container(height: 10),
      itemBuilder: (context, index) => _buildNews(),
      itemCount: 10,
      padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),
      physics: BouncingScrollPhysics(),
    );
  }

  Widget _buildNews() {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, Routers.news),
      child: Column(
        children: [
          PhysicalModel(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: AppColors.grey,
            elevation: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              child: Image.asset(
                AppImages.imgBg2,
                width: AppSize.screenWidth,
                height: 170,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Container(
            width: AppSize.screenWidth,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(5),
                bottomRight: Radius.circular(5),
              ),
              color: AppColors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 5),
                Text(
                  "Biệt thự đảo - Xu hướng mới trên thị trường bất động sản siêu sang.",
                  style: AppStyles.DEFAULT_LARGE.copyWith(
                    fontSize: 17,
                  ),
                ),
                const SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "23/1/2021",
                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                        color: AppColors.grey,
                      ),
                    ),
                    GestureDetector(
                      onTap: () => Navigator.pushNamed(context, Routers.news_detail),
                      child: Text(
                        AppLocalizations.of(context).translate("see_detail"),
                        style: AppStyles.DEFAULT_MEDIUM.copyWith(
                          color: AppColors.red,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTabBar() {
    return Column(
      children: [
        Container(
          height: 60,
          child: ListView.separated(
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.only(top: 10, left: 15, right: 15, bottom: 10),
            separatorBuilder: (context, index) => Container(width: 10),
            itemBuilder: (context, index) => createTab(
              translateText: "all",
              index: index,
            ),
            itemCount: 3,
          ),
        ),
      ],
    );
  }

  Widget createTab({String translateText, int index}) {
    return GestureDetector(
      onTap: () => _viewModel.switchTab(index),
      child: Container(
        width: 110,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50)),
          color: _viewModel.exactlyColor(index),
        ),
        child: Center(
          child: new Text(
            AppLocalizations.of(context).translate(translateText),
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.white,
              fontSize: 16,
            ),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ),
      ),
    );
  }
}

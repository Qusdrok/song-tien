import 'package:flutter/material.dart';

import '../../src.dart';

class NewsViewModel extends BaseViewModel {
  int currentPage = 0;

  init() {}

  switchTab(int index) {
    currentPage = index;
    notifyListeners();
  }

  exactlyColor(int index) {
    return currentPage == index ? AppColors.red : Colors.grey;
  }
}

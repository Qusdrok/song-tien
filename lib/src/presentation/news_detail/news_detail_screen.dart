import 'package:flutter/material.dart';

import '../../src.dart';

class NewsDetailScreen extends StatefulWidget {
  @override
  _NewsDetailScreenState createState() => _NewsDetailScreenState();
}

class _NewsDetailScreenState extends State<NewsDetailScreen> {
  NewsDetailViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NewsDetailViewModel>(
      viewModel: NewsDetailViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  colorButton: AppColors.white,
                  colorTitle: AppColors.white,
                  keyTitle: "detail_news",
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset(
            AppImages.imgBg2,
            width: AppSize.screenWidth,
            height: 200,
            fit: BoxFit.fill,
          ),
          Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Biệt thự đảo - Xu hướng mới trên thị trường bất động sản siêu sang.",
                  style: AppStyles.DEFAULT_LARGE.copyWith(
                    color: AppColors.black,
                  ),
                ),
                const SizedBox(height: 5),
                Text(
                  "23/1/2021",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.grey,
                  ),
                ),
                const SizedBox(height: 5),
                Text(
                  "Biệt thự đảo - Xu hướng mới trên thị trường bất động sản siêu sang.",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.black,
                  ),
                ),
                const SizedBox(height: 15),
                Image.asset(
                  AppImages.imgBg2,
                  width: AppSize.screenWidth,
                  height: 200,
                  fit: BoxFit.fill,
                ),
                const SizedBox(height: 10),
                Text(
                  "Biệt thự đảo - Xu hướng mới trên thị trường bất động sản siêu sang.",
                  style: AppStyles.DEFAULT_SMALL.copyWith(
                    color: AppColors.black,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 10),
                Text(
                  "Biệt thự đảo - Xu hướng mới trên thị trường bất động sản siêu sang.",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.black,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

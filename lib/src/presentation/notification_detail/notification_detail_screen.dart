import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class NotificationDetailScreen extends StatefulWidget {
  final NotificationModel model;

  NotificationDetailScreen({this.model});

  @override
  _NotificationDetailScreenState createState() => _NotificationDetailScreenState();
}

class _NotificationDetailScreenState extends State<NotificationDetailScreen> {
  NotificationDetailViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NotificationDetailViewModel>(
      viewModel: NotificationDetailViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Chi Tiết Thông Báo",
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: DottedBorder(
        borderType: BorderType.RRect,
        radiusBottomLeft: Radius.circular(5),
        radiusBottomRight: Radius.circular(5),
        radiusTopLeft: Radius.circular(5),
        radiusTopRight: Radius.circular(5),
        dashPattern: [10, 7],
        color: AppColors.primaryDark,
        child: Container(
          height: AppSize.screenHeight,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            color: Colors.white,
          ),
          padding: const EdgeInsets.all(15.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  AppImages.imgBgLogin,
                  width: AppSize.screenWidth - 45,
                  height: 180,
                  fit: BoxFit.fill,
                ),
                const SizedBox(height: 10),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    widget.model.title,
                    style: AppStyles.DEFAULT_LARGE_BOLD,
                  ),
                ),
                const SizedBox(height: 10),
                Text(
                  widget.model.content,
                  style: AppStyles.DEFAULT_MEDIUM,
                ),
                const SizedBox(height: 15),
                Text(
                  AppUtils.convertDateTime2String(widget.model.time),
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.grey,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

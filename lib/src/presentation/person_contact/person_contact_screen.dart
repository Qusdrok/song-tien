import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../src.dart';

class PersonContactScreen extends StatefulWidget {
  @override
  _PersonContactScreenState createState() => _PersonContactScreenState();
}

class _PersonContactScreenState extends State<PersonContactScreen> {
  PersonContactViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PersonContactViewModel>(
      viewModel: PersonContactViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Người Liên Hệ",
                  trans: false,
                  actions: [
                    IconButton(
                      icon: Icon(
                        Icons.add_box,
                        color: AppColors.white,
                      ),
                      onPressed: () => Navigator.pushNamed(context, Routers.person_contact_add),
                    ),
                  ],
                ),
                _buildTextField(
                  controller: _viewModel.searchController,
                  hintText: "Tìm kiếm ...",
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: EdgeInsets.only(top: 12, left: 12, right: 12),
      child: StreamBuilder(
        stream: _viewModel.loadingSubject,
        builder: (context, snapshot) {
          var data = _viewModel.resultSearch;
          bool enabled = data == null;

          return enabled
              ? Shimmer.fromColors(
                  child: ListView.separated(
                    separatorBuilder: (context, index) => Container(height: 10),
                    itemBuilder: (context, index) => _buildValue(null),
                    itemCount: 10,
                  ),
                  baseColor: AppColors.grey,
                  highlightColor: Colors.white,
                )
              : data.length > 0
                  ? ListView.separated(
                      separatorBuilder: (context, index) => Container(height: 10),
                      itemCount: data.length,
                      itemBuilder: (context, index) => _buildValue(data[index]),
                    )
                  : _viewModel.searchController.text.isEmpty
                      ? Container()
                      : Text(
                          "Không có kết quả",
                          style: AppStyles.DEFAULT_LARGE.copyWith(color: AppColors.black),
                          textAlign: TextAlign.center,
                        );
        },
      ),
    );
  }

  Widget _buildValue(String value) {
    return GestureDetector(
      onTap: () => Navigator.pop(context, value),
      child: Container(
        width: AppSize.screenWidth,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
        ),
        padding: EdgeInsets.only(left: 12, right: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                value ?? "Kết quả",
                style: AppStyles.DEFAULT_MEDIUM,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Text(
              "0364119999",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextField({TextEditingController controller, String hintText}) {
    return Material(
      elevation: 5,
      shadowColor: AppColors.grey,
      child: TextFormField(
        controller: controller,
        onFieldSubmitted: _viewModel.controlSearching,
        decoration: InputDecoration(
          suffixIcon: PhysicalModel(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            elevation: 6,
            child: Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: AppColors.red,
              ),
              child: Center(
                child: Icon(
                  Icons.search,
                  size: 50,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              color: Colors.transparent,
              width: 1.2,
            ),
          ),
          contentPadding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          filled: true,
          fillColor: Colors.white,
          hintText: hintText,
          hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.grey,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';

import '../../src.dart';

class PersonContactViewModel extends BaseViewModel {
  TextEditingController searchController = TextEditingController();
  StreamController searchStream = StreamController<List<String>>();

  List<String> citys = [];
  List<String> resultSearch = [];

  init() {
    citys = [
      "Hà Nội",
      "Hải Phòng",
      "Thanh Hoá",
      "Thái Bình",
      "Quảng Ninh",
      "Huế",
    ];
  }

  controlSearching(String keyword) {
    getResultSearch(keyword);
  }

  getResultSearch(String keyword) {
    setLoading(true);
    resultSearch.clear();
    for (var s in citys) if (s.toLowerCase().contains(keyword)) resultSearch.add(s);
    searchStream.sink.add(resultSearch);
    setLoading(false);
  }
}

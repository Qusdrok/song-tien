import 'package:flutter/material.dart';

import '../../src.dart';

class PersonContactAddScreen extends StatefulWidget {
  @override
  _PersonContactAddScreenState createState() => _PersonContactAddScreenState();
}

class _PersonContactAddScreenState extends State<PersonContactAddScreen> {
  PersonContactAddViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PersonContactAddViewModel>(
      viewModel: PersonContactAddViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Thêm người liên hệ",
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: EdgeInsets.all(12),
      child: Column(
        children: [
          Form(
            key: _viewModel.formPersonContact,
            child: Column(
              children: [
                _buildTextField(
                  controller: _viewModel.nameController,
                  hintText: "Họ tên",
                ),
                const SizedBox(height: 7),
                _buildTextField(
                  controller: _viewModel.phoneController,
                  hintText: "Số điện thoại",
                ),
              ],
            ),
          ),
          Spacer(),
          WidgetButtonGradientAni(
            width: AppSize.screenWidth,
            title: "Lưu lại",
            trans: false,
            colorStart: AppColors.primaryDark,
            colorEnd: AppColors.primaryDark,
            action: () {},
            textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.white,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildTextField({
    TextEditingController controller,
    String hintText,
  }) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(Colors.transparent),
        contentPadding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../../src.dart';

class PrivacyPolicyScreen extends StatefulWidget {
  @override
  _PrivacyPolicyScreenState createState() => _PrivacyPolicyScreenState();
}

class _PrivacyPolicyScreenState extends State<PrivacyPolicyScreen> {
  PrivacyPolicyViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PrivacyPolicyViewModel>(
      viewModel: PrivacyPolicyViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Điều Khoản Chính Sách",
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Lorem Ipsum ádjasjdgjasgdjgajsdgjagsdjasgdjahsgdjagsjd"
            "hgjasgdjasgjdhgajshdgajshgdjahgsdjhagsjdhgasjdhgjahsgdhasdhg"
            "hgjasgdjasgjdhgajshdgajshgdjahgsdjhagsjdhgasjdhgjahsgdhasdhg"
            "hgjasgdjasgjdhgajshdgajshgdjahgsdjhagsjdhgasjdhgjahsgdhasdhg"
            "hgjasgdjasgjdhgajshdgajshgdjahgsdjhagsjdhgasjdhgjahsgdhasdhg",
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              fontSize: 17,
            ),
          ),
          _buildLogo(),
        ],
      ),
    );
  }

  Widget _buildLogo() {
    return Column(
      children: [
        WidgetLogo(),
        const SizedBox(height: 3),
        Text(
          "Copyright STC",
          style: AppStyles.DEFAULT_MEDIUM,
        ),
      ],
    );
  }
}

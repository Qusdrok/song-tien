import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class ReservationScreen extends StatefulWidget {
  @override
  _ReservationScreenState createState() => _ReservationScreenState();
}

class _ReservationScreenState extends State<ReservationScreen> {
  ReservationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ReservationViewModel>(
      viewModel: ReservationViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Đặt Chỗ",
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildHeader("Thông tin đặt chỗ"),
          const SizedBox(height: 15),
          _buildRowInfo(header: "Mã chuyến", content: "C001"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Người liên hệ", content: "Nguyễn Hằng"),
          if (_viewModel.privilege == Privilege.Admin)
            Column(
              children: [
                const SizedBox(height: 7),
                _buildRowInfo(header: "Người đặt", content: "Nguyễn Hằng"),
              ],
            ),
          const SizedBox(height: 7),
          _buildRowInfo(
            header: "Thời gian đặt",
            content: AppUtils.convertDateTime2String(DateTime.now()),
          ),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Số chỗ", content: "2"),
          const SizedBox(height: 7),
          Container(
            height: 85,
            width: AppSize.screenWidth,
            child: _buildTextField(hintText: "Ghi chú"),
          ),
          const SizedBox(height: 15),
          _buildHeader("Thông tin hành khách"),
          const SizedBox(height: 15),
          _buildCustomerInfo(
            slot: "A1",
            name: "Huyền Trương",
            phone: "03641199",
          ),
          const SizedBox(height: 7),
          _buildCustomerInfo(
            slot: "A1",
            name: "Huyền Trương",
            phone: "03641199",
          ),
          const SizedBox(height: 15),
          _buildHeader("Thông tin thanh toán"),
          const SizedBox(height: 15),
          _buildRowInfo(header: "Giá vé", content: "${AppUtils.convertNumber(2500000)} đ"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Số lượng", content: "2"),
          const SizedBox(height: 15),
          _buildRowInfo(header: "Thành tiền", content: "${AppUtils.convertNumber(5000000)} đ"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "VAT (10%)", content: "${AppUtils.convertNumber(500000)} đ"),
          const SizedBox(height: 7),
          _buildRowInfo(
            header: "Tổng",
            content: "${AppUtils.convertNumber(5500000)} đ",
            colorHeader: AppColors.primaryDark,
          ),
          const SizedBox(height: 15),
          RichText(
            text: TextSpan(
              text: '*',
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: AppColors.red,
              ),
              children: [
                TextSpan(
                  text: ' Khách hàng thành toán sau tại quầy',
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.black,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 15),
          WidgetButtonGradientAni(
            width: AppSize.screenWidth,
            title: "Hoàn tất",
            trans: false,
            colorStart: AppColors.primaryDark,
            colorEnd: AppColors.primaryDark,
            action: () => Navigator.pushNamed(context, Routers.reservation_info),
            textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader(String header) {
    return Text(
      header,
      style: AppStyles.DEFAULT_MEDIUM_BOLD,
    );
  }

  Widget _buildCustomerInfo({
    String slot,
    String name,
    String phone,
  }) {
    return Column(
      children: [
        Container(
          width: AppSize.screenWidth,
          height: 40,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            color: Colors.white,
          ),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  slot,
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.primaryDark,
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    name,
                    style: AppStyles.DEFAULT_MEDIUM,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    phone,
                    style: AppStyles.DEFAULT_MEDIUM,
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 7),
        if (_viewModel.privilege == Privilege.Admin)
          Container(
            height: 85,
            width: AppSize.screenWidth,
            child: _buildTextField(hintText: "Ghi chú"),
          ),
      ],
    );
  }

  Widget _buildRowInfo({
    String header,
    String content,
    Color colorHeader,
  }) {
    return Container(
      width: AppSize.screenWidth,
      height: 50,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Expanded(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                header,
                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: colorHeader ?? AppColors.grey,
                ),
              ),
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                content,
                style: AppStyles.DEFAULT_MEDIUM,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTextField({String hintText}) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.top,
      expands: true,
      maxLines: null,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        contentPadding: EdgeInsets.all(10),
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(Colors.transparent),
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

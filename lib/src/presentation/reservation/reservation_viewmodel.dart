import '../../src.dart';

class ReservationViewModel extends BaseViewModel {
  Privilege privilege;

  init() async {
    setLoading(true);
    privilege = (await AppShared.getUser()).privilege;
    setLoading(false);
  }
}

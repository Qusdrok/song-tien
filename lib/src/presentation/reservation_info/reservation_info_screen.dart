import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class ReservationInfoScreen extends StatefulWidget {

  @override
  _ReservationInfoScreenState createState() => _ReservationInfoScreenState();
}

class _ReservationInfoScreenState extends State<ReservationInfoScreen> {
  ReservationInfoViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ReservationInfoViewModel>(
      viewModel: ReservationInfoViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Thông Tin Đặt Chỗ",
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildHeader("Thông tin đặt chỗ"),
          const SizedBox(height: 15),
          _buildRowInfo(header: "Mã chuyến", content: "C001"),
          const SizedBox(height: 7),
          if (_viewModel.user.privilege == Privilege.Moderator)
            _buildRowInfo(header: "Người liên hệ", content: "Nguyễn Hằng")
          else if (_viewModel.user.privilege == Privilege.Admin)
            Column(
              children: [
                GestureDetector(
                  onTap: () => Navigator.pushNamed(context, Routers.person_contact),
                  child: _buildRowInfo(
                    header: "Người liên hệ",
                    child: Icon(
                      Icons.arrow_drop_down,
                      color: AppColors.primaryDark,
                    ),
                  ),
                ),
                const SizedBox(height: 7),
                _buildRowInfo(header: "Người đặt", content: ""),
              ],
            ),
          const SizedBox(height: 7),
          Container(
            height: 85,
            width: AppSize.screenWidth,
            child: _buildTextField(hintText: "Ghi chú"),
          ),
          const SizedBox(height: 15),
          _buildHeader("Thông tin hành khách"),
          const SizedBox(height: 15),
          _buildRowInfo(header: "Mã chỗ ngồi", content: "A1"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Tên khách hàng", content: ""),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Số điện thoại", content: ""),
          const SizedBox(height: 15),
          _buildRowInfo(header: "Mã chỗ ngồi", content: "A1"),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Tên khách hàng", content: ""),
          const SizedBox(height: 7),
          _buildRowInfo(header: "Số điện thoại", content: ""),
          SizedBox(height: AppSize.screenWidth / 3),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              WidgetButtonGradient(
                width: AppSize.screenWidth / 2 - 25,
                title: "Thêm khách hàng",
                colorStart: Colors.white,
                colorEnd: Colors.white,
                colorTitle: AppColors.primaryDark,
                colorBorder: AppColors.primaryDark,
                action: () => _buildBottomSheet(),
              ),
              WidgetButtonGradient(
                width: AppSize.screenWidth / 2 - 25,
                title: "Đặt chỗ",
                colorStart: AppColors.primary,
                colorEnd: AppColors.primary,
                action: () => Navigator.pushNamed(context, Routers.reservation),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildBottomSheet() {
    showModalBottomSheet(
      backgroundColor: AppColors.white,
      context: context,
      builder: (c) {
        return Container(
          height: AppSize.screenHeight / 2 + 5,
          padding: EdgeInsets.only(top: 16, left: 15, right: 15),
          child: Column(
            children: [
              Text(
                "Thêm chỗ ngồi",
                style: AppStyles.DEFAULT_MEDIUM_BOLD,
              ),
              const SizedBox(height: 20),
              PhysicalModel(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                elevation: 3,
                color: AppColors.grey,
                child: Container(
                  width: AppSize.screenWidth,
                  height: 200,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Colors.white,
                  ),
                  child: SingleChildScrollView(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        _buildChair(chairs: _viewModel.chair1),
                        _buildChair(chairs: _viewModel.chair1),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 12),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: WidgetButtonGradientAni(
                  width: AppSize.screenWidth,
                  title: "Thêm chỗ ngồi",
                  trans: false,
                  colorStart: AppColors.primaryDark,
                  colorEnd: AppColors.primaryDark,
                  action: () => Navigator.pushNamed(context, Routers.reservation_info),
                  textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.white,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildChair({List<ChairModel> chairs}) {
    double padding = AppSize.screenWidth / 2 - 130;
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: padding, right: padding, top: 15, bottom: 15),
        child: Wrap(
          spacing: 5,
          runSpacing: 5,
          children: List.generate(
            chairs.length,
            (index) => Stack(
              alignment: Alignment.center,
              children: [
                Icon(
                  Icons.event_seat,
                  size: 30,
                  color: chairs[index].chairColor(),
                ),
                Text(
                  chairs[index].name,
                  style: AppStyles.DEFAULT_SMALL.copyWith(
                    color: AppColors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildHeader(String header) {
    return Text(
      header,
      style: AppStyles.DEFAULT_MEDIUM_BOLD,
    );
  }

  Widget _buildRowInfo({String header, String content, Widget child}) {
    return Container(
      width: AppSize.screenWidth,
      height: 50,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Expanded(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                header,
                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: AppColors.grey,
                ),
              ),
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: child ??
                  Text(
                    content,
                    style: AppStyles.DEFAULT_MEDIUM,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTextField({String hintText}) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.top,
      expands: true,
      maxLines: null,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        contentPadding: EdgeInsets.all(10),
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(Colors.transparent),
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

import '../../src.dart';

class ReservationInfoViewModel extends BaseViewModel {
  List<ChairModel> chair1 = [];
  UserModel user;

  init() async {
    setLoading(true);
    user = await AppShared.getUser();
    chair1 = [
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
    ];
    setLoading(false);
  }
}

import 'package:flutter/material.dart';

import '../src.dart';

class Routers {
  static const String navigation = "/navigation";
  static const String login = "/login";
  static const String account = "/account";
  static const String account_update = "/account_update";
  static const String forgot_password = "/forgot_password";
  static const String update_password = "/update_password";
  static const String news = "/news";
  static const String search = "/search";
  static const String splash = "/splash";
  static const String notification = "/notification";
  static const String notification_detail = "/notification_detail";
  static const String news_detail = "/news_detail";
  static const String tour_detail = "/tour_detail";
  static const String tour_more_detail = "/tour_more_detail";
  static const String reservation_info = "/reservation_info";
  static const String customer_info = "/customer_info";
  static const String customer_info_edit = "/customer_info_edit";
  static const String reservation = "/reservation";
  static const String enter_otp = "/enter_otp";
  static const String change_password = "/change_password";
  static const String privacy_policy = "/privacy_policy";
  static const String person_contact = "/person_contact";
  static const String person_contact_add = "/person_contact_add";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    var arguments = settings.arguments;
    switch (settings.name) {
      case navigation:
        return animRoute(NavigationScreen());
      case login:
        return animRoute(LoginScreen());
      case forgot_password:
        return animRoute(ForgotPasswordScreen());
      case update_password:
        return animRoute(UpdatePasswordScreen());
      case news:
        return animRoute(NewsScreen());
      case news_detail:
        return animRoute(NewsDetailScreen());
      case account:
        return animRoute(AccountScreen());
      case tour_detail:
        return animRoute(TourDetailScreen(tourDetail: arguments));
      case tour_more_detail:
        return animRoute(TourMoreDetailScreen(tourDetail: arguments));
      case reservation_info:
        return animRoute(ReservationInfoScreen());
      case customer_info:
        return animRoute(CustomerInfoScreen());
      case reservation:
        return animRoute(ReservationScreen());
      case enter_otp:
        return animRoute(EnterOTPScreen(phoneNumber: arguments));
      case account:
        return animRoute(AccountScreen());
      case account_update:
        return animRoute(AccountUpdateScreen());
      case search:
        return animRoute(SearchScreen(searchType: arguments));
      case notification:
        return animRoute(NotificationScreen());
      case splash:
        return animRoute(SplashScreen());
      case privacy_policy:
        return animRoute(PrivacyPolicyScreen());
      case person_contact:
        return animRoute(PersonContactScreen());
      case person_contact_add:
        return animRoute(PersonContactAddScreen());
      case change_password:
        return animRoute(ChangePasswordScreen());
      case notification_detail:
        return animRoute(NotificationDetailScreen(model: arguments));
      case customer_info_edit:
        return animRoute(CustomerInfoEditScreen());
      default:
        return animRoute(
          Container(
            child: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }

  static Route animRoute(Widget page, {Offset beginOffset, String name, Object arguments}) {
    return PageRouteBuilder(
      settings: RouteSettings(name: name, arguments: arguments),
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = beginOffset ?? Offset(1.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Route scaleRoute(Widget page) {
    return PageRouteBuilder(
      transitionDuration: Duration(milliseconds: 500),
      pageBuilder: (context, animation, secondAnimation) => page,
      transitionsBuilder: (context, animation, secondAnimation, child) {
        return ScaleTransition(
          scale: animation,
          alignment: Alignment.center,
          child: child,
        );
      },
    );
  }

  static Offset _center = Offset(0.0, 0.0);
  static Offset _top = Offset(0.0, 1.0);
  static Offset _bottom = Offset(0.0, -1.0);
  static Offset _left = Offset(-1.0, 0.0);
  static Offset _right = Offset(1.0, 0.0);
}

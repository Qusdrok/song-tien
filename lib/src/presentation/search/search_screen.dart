import 'package:flutter/material.dart';

import '../../src.dart';

enum SearchType { District, Province }

class SearchScreen extends StatefulWidget {
  final SearchType searchType;

  SearchScreen({this.searchType});

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  ScrollController _scrollController = ScrollController();
  SearchViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset) {}
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SearchViewModel>(
      viewModel: SearchViewModel(searchType: widget.searchType),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: _viewModel.title,
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: EdgeInsets.only(top: 12, left: 12, right: 12),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildTextField(
            controller: _viewModel.searchController,
            hintText: _viewModel.title,
          ),
          const SizedBox(height: 10),
          Expanded(
            child: StreamBuilder(
              stream: _viewModel.loadingSubject,
              builder: (context, snapshot) {
                var data = _viewModel.resultSearch;
                bool enabled = data == null;

                return enabled
                    ? WidgetShimmer(
                        child: ListView.separated(
                          separatorBuilder: (context, index) => Container(height: 10),
                          itemBuilder: (context, index) => _buildValue(null),
                          itemCount: 10,
                        ),
                      )
                    : data.length > 0
                        ? ListView.separated(
                            separatorBuilder: (context, index) => Container(height: 10),
                            itemCount: data.length,
                            physics: BouncingScrollPhysics(),
                            padding: EdgeInsets.only(bottom: 10),
                            itemBuilder: (context, index) => _buildValue(data[index]),
                          )
                        : _viewModel.searchController.text.isEmpty
                            ? Text(
                                "Bạn chưa nhập từ khoá để tìm kiếm",
                                style: AppStyles.DEFAULT_LARGE.copyWith(color: AppColors.black),
                                textAlign: TextAlign.center,
                              )
                            : Text(
                                "Không có kết quả",
                                style: AppStyles.DEFAULT_LARGE.copyWith(color: AppColors.black),
                                textAlign: TextAlign.center,
                              );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildValue(String value) {
    return GestureDetector(
      onTap: () => Navigator.pop(context, value),
      child: Container(
        width: AppSize.screenWidth,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
        ),
        padding: EdgeInsets.only(left: 12),
        alignment: Alignment.centerLeft,
        child: Text(
          value ?? "Kết quả",
          style: AppStyles.DEFAULT_MEDIUM,
        ),
      ),
    );
  }

  Widget _buildTextField({TextEditingController controller, String hintText}) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        suffixIcon: GestureDetector(
          onTap: _viewModel.controlSearching,
          child: PhysicalModel(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            elevation: 6,
            child: Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: AppColors.red,
              ),
              child: Center(
                child: Icon(
                  Icons.search,
                  size: 50,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(Colors.transparent),
        contentPadding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

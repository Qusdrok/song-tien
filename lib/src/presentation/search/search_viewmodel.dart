import 'dart:async';

import 'package:flutter/material.dart';

import '../../src.dart';

class SearchViewModel extends BaseViewModel {
  TextEditingController searchController = TextEditingController();
  StreamController searchStream = StreamController<List<String>>();

  List<String> resultSearch = [];

  SearchType searchType;
  String title;

  SearchViewModel({this.searchType});

  init() => title = getTitle();

  String getTitle() {
    switch (searchType) {
      case SearchType.District:
        return "Quận / huyện";
      case SearchType.Province:
        return "Tỉnh / thành phố";
    }
  }

  Future<void> controlSearching() async {
    unFocus();
    setLoading(true);

    var targets = [];
    resultSearch = null;

    switch (searchType) {
      case SearchType.District:
        targets = await getDistricts();
        break;
      case SearchType.Province:
        targets = await getProvinces();
        break;
    }

    resultSearch = [];
    for (var s in targets) {
      var str;
      try {
        str = s.name;
      } catch (e) {
        str = s.nameWithType;
      }

      if (str.toLowerCase().contains(searchController.text)) resultSearch.add(str);
    }

    searchStream.sink.add(resultSearch);
    setLoading(false);
  }

  Future<List<ProvinceModel>> getProvinces() async {
    List<ProvinceModel> data = [];
    NetworkState state = await authRepository.getProvinces();
    if (state.data != null && state.isSuccess) data = state.data;
    return data;
  }

  Future<List<DistrictModel>> getDistricts() async {
    List<DistrictModel> data = [];
    NetworkState state = await authRepository.getDistricts();
    if (state.data != null && state.isSuccess) data = state.data;
    return data;
  }
}

import 'package:flutter/material.dart';

import '../../src.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  SplashViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget(
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, builder, child) {
        return Scaffold(
          backgroundColor: AppColors.primaryLight,
          body: SafeArea(
            child: Center(
              child: _buildBody(),
            ),
          ),
        );
      },
      viewModel: SplashViewModel(),
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.only(left: 25, right: 20),
      child: Row(
        children: [
          Expanded(
            child: Image.asset(
              AppImages.Logo,
              height: 55,
              fit: BoxFit.fill,
            ),
          ),
          const SizedBox(width: 12),
          Container(
            height: 50,
            width: 1.8,
            color: AppColors.primary,
          ),
          const SizedBox(width: 12),
          Expanded(
            child: Text(
              "Sông Tiên Corporation",
              style: AppStyles.DEFAULT_LARGE.copyWith(
                color: AppColors.primary,
                fontSize: 25,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

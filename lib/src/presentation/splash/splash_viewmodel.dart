import 'package:flutter/material.dart';

import '../../src.dart';

class SplashViewModel extends BaseViewModel {
  init() async {
    setLoading(true);
    AppFirebase.firebaseAuth.authStateChanges().listen(
      (user) async {
        if (user != null) {
          Navigator.pushReplacementNamed(context, Routers.navigation);
        } else {
          Navigator.pushReplacementNamed(context, Routers.login);
        }
      },
    );

    await Future.delayed(Duration(milliseconds: 750));
    setLoading(false);
  }
}

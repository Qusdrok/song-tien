import 'package:flutter/material.dart';

import '../../src.dart';

class TourScreen extends StatefulWidget {
  @override
  _TourScreenState createState() => _TourScreenState();
}

class _TourScreenState extends State<TourScreen> {
  TourViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TourViewModel>(
      viewModel: TourViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.grey.withOpacity(0.5),
          body: Column(
            children: [
              WidgetAppBar(
                keyTitle: "Chương Trình Tham Quan",
                isBack: false,
                trans: false,
              ),
              Expanded(child: _buildBody()),
            ],
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    print(_viewModel.tours.length);
    return ListView.separated(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.symmetric(vertical: 8),
      itemBuilder: (context, index) => _buildTour(tour: _viewModel.tours[index]),
      separatorBuilder: (context, index) => Container(height: 8),
      itemCount: _viewModel.tours.length,
    );
  }

  Widget _buildTour({TourModel tour}) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        Routers.tour_detail,
        arguments: TourDetailModel(
          tours: _viewModel.tours,
          user: _viewModel.user,
          tour: tour,
        ),
      ),
      child: Container(
        width: AppSize.screenWidth,
        height: 135,
        padding: EdgeInsets.only(left: 10, right: 10, top: 8, bottom: 8),
        color: AppColors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            PhysicalModel(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: AppColors.grey,
              elevation: 6,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.asset(
                  tour.imageUrl,
                  width: 125,
                  height: 140,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            const SizedBox(width: 12),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: AppSize.screenWidth - 165,
                  child: Text(
                    tour.name,
                    style: AppStyles.DEFAULT_LARGE.copyWith(
                      color: AppColors.primaryDark,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                const SizedBox(height: 5),
                _buildStar(tour.star),
                const SizedBox(height: 8),
                Text(
                  "Mã số chương trình: ${tour.code}",
                  style: AppStyles.DEFAULT_SMALL,
                ),
                const SizedBox(height: 5),
                Text(
                  "Loại: ${tour.type}",
                  style: AppStyles.DEFAULT_SMALL,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildStar(int star) {
    return Wrap(
      spacing: 3,
      children: List.generate(
        star,
        (index) => Icon(
          Icons.star,
          size: 14,
          color: AppColors.orangeLight,
        ),
      ),
    );
  }
}

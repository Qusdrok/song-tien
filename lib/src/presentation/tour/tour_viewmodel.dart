import '../../src.dart';

class TourViewModel extends BaseViewModel {
  List<TourModel> tours = [];
  UserModel user;

  init() async {
    tours = [
      TourModel(
        name: "Tham quan đảo Angel Island bằng du thuyền",
        star: 5,
        type: "VIP",
        imageUrl: AppImages.imgBg2,
        code: "T001",
      ),
      TourModel(
        name: "Tham quan đảo Angel Island bằng du thuyền 2",
        star: 5,
        type: "Thường",
        imageUrl: AppImages.imgBg2,
        code: "T002",
      ),
      TourModel(
        name: "Tham quan đảo Angel Island bằng du thuyền 3",
        star: 5,
        type: "VIP",
        imageUrl: AppImages.imgBg2,
        code: "T001",
      ),
      TourModel(
        name: "Tham quan đảo Angel Island bằng du thuyền 4",
        star: 5,
        type: "Thường",
        imageUrl: AppImages.imgBg2,
        code: "T002",
      ),
    ];

    setLoading(true);
    user = await AppShared.getUser();
    user.privilege = user.getPrivilege();
    setLoading(false);
  }
}

import 'package:flutter/material.dart';

import '../../src.dart';

class TourDetailScreen extends StatefulWidget {
  final TourDetailModel tourDetail;

  TourDetailScreen({this.tourDetail});

  @override
  _TourDetailScreenState createState() => _TourDetailScreenState();
}

class _TourDetailScreenState extends State<TourDetailScreen> with SingleTickerProviderStateMixin {
  TabController _tabController;
  TourDetailViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TourDetailViewModel>(
      viewModel: TourDetailViewModel(model: widget.tourDetail),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: _buildBody(),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    List<String> images = [];
    for (var x in _viewModel.model.tours) {
      images.add(x.imageUrl);
    }

    return SingleChildScrollView(
      child: Stack(
        children: [
          Column(
            children: [
              Stack(
                overflow: Overflow.visible,
                children: [
                  WidgetCarousel(
                    images: images,
                    circular: 0,
                    onPageChanged: _viewModel.setSelected,
                  ),
                  Positioned(
                    left: 12,
                    right: 12,
                    bottom: -40,
                    child: _buildTour(),
                  ),
                ],
              ),
              const SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 12, right: 12, bottom: 12),
                child: AnimatedSwitcher(
                  transitionBuilder: (child, animation) => RotationTransition(turns: animation, child: child),
                  layoutBuilder: (widget, list) => Stack(children: [widget, ...list]),
                  switchInCurve: Curves.easeInBack,
                  switchOutCurve: Curves.easeInBack.flipped,
                  duration: Duration(seconds: 1),
                  child: GestureDetector(
                    onTap: _viewModel.unFocus,
                    child: StreamBuilder<bool>(
                      stream: _viewModel.loadingSubject,
                      builder: (context, snapshot) {
                        return Container(
                          width: AppSize.screenWidth,
                          color: Colors.transparent,
                          child: !_viewModel.addTour ? _buildTourDetail() : _buildAddTour(),
                        );
                      },
                    ),
                  ),
                ),
              ),
              if (_viewModel.addTour)
                Container(
                  width: AppSize.screenWidth,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.grey.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 2,
                      ),
                    ],
                    color: Colors.white,
                  ),
                  padding: EdgeInsets.all(10),
                  child: WidgetButtonGradientAni(
                    width: AppSize.screenWidth,
                    title: "Lưu",
                    trans: false,
                    colorStart: AppColors.primaryDark,
                    colorEnd: AppColors.primaryDark,
                    action: () => Navigator.pushNamed(context, Routers.reservation_info),
                    textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: AppColors.white,
                    ),
                  ),
                ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              WidgetButtonBack(
                color: Colors.white,
                size: 18,
                action: () => _viewModel.addTour ? _viewModel.switchAddTour() : Navigator.pop(context),
              ),
              if (!_viewModel.addTour && widget.tourDetail.user.privilege == Privilege.Admin)
                IconButton(
                  icon: Icon(Icons.add_box),
                  color: Colors.white,
                  iconSize: 18,
                  onPressed: () => _viewModel.switchAddTour(),
                ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildAddTour() {
    return Column(
      children: [
        const SizedBox(height: 15),
        Text(
          "TẠO CHUYẾN THAM QUAN",
          style: AppStyles.DEFAULT_LARGE.copyWith(
            color: AppColors.primaryDark,
          ),
        ),
        const SizedBox(height: 12),
        _buildRowInfo(header: "Mã số Chương Trình", content: "T001"),
        const SizedBox(height: 7),
        _buildTextField(controller: _viewModel.nameTourController, hintText: "Tên chuyến"),
        const SizedBox(height: 7),
        GestureDetector(
          onTap: () {},
          child: _buildRowInfo(
            header: "Ngày khởi hành",
            content: "23/02/20201ạksdkjashkdjhkasjdjaks",
            child: Icon(
              Icons.arrow_drop_down,
              size: 25,
              color: AppColors.primaryDark,
            ),
          ),
        ),
        const SizedBox(height: 7),
        _buildTextField(controller: _viewModel.timeStartController, hintText: "Giờ khởi hành"),
        const SizedBox(height: 7),
        _buildTextField(controller: _viewModel.timeStartController, hintText: "Giờ kết thúc"),
        const SizedBox(height: 7),
        GestureDetector(
          onTap: () {},
          child: _buildRowInfo(
            header: "Mã số tàu - Tên",
            child: Icon(
              Icons.arrow_drop_down,
              size: 25,
              color: AppColors.primaryDark,
            ),
          ),
        ),
        const SizedBox(height: 7),
        _buildTextField(controller: _viewModel.nameTourController, hintText: "Tên chuyến"),
        const SizedBox(height: 7),
        _buildTextField(controller: _viewModel.addressController, hintText: "Địa điểm xuất bến"),
        const SizedBox(height: 7),
        GestureDetector(
          onTap: () {},
          child: _buildRowInfo(
            header: "Người hướng dẫn",
            child: Icon(
              Icons.arrow_drop_down,
              size: 25,
              color: AppColors.primaryDark,
            ),
          ),
        ),
        const SizedBox(height: 7),
        _buildTextField(controller: _viewModel.moneyController, hintText: "Phí (Giá vé)"),
      ],
    );
  }

  Widget _buildTourDetail() {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      color: Colors.white,
      elevation: 3,
      child: Column(
        children: [
          _buildTabBar(
            tabs: [
              _buildTab(title: "Nội dung\ntour", index: 0),
              _buildTab(title: "Các chuyến\nđang thực hiện", index: 1),
              _buildTab(title: "Các chuyến\nđã thực hiện", index: 2),
            ],
          ),
          Container(
            width: AppSize.screenWidth,
            height: AppSize.screenHeight / 2,
            child: TabBarView(
              controller: _tabController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                _buildTourDetailView1(),
                _buildTourDetailView2(),
                _buildTourDetailView2(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTourDetailView1() {
    return SingleChildScrollView(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTitle(
            title: "Mã số chương trình:",
            content: _viewModel.model.tour.code,
          ),
          const SizedBox(height: 5),
          _buildTitle(
            title: "Loại:",
            content: _viewModel.model.tour.type,
          ),
          const SizedBox(height: 5),
          Text(
            AppLocalizations.of(context).translate("tour_content_2"),
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.primaryDark,
              fontSize: 17,
            ),
          ),
          const SizedBox(height: 8),
        ],
      ),
    );
  }

  Widget _buildTourDetailView2() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          WidgetTourDetailInfo(
            title: "05/02/2021 - C0000000001 - Chuyến tham quan",
            onTapInfo: () => Navigator.pushNamed(
              context,
              Routers.tour_more_detail,
              arguments: widget.tourDetail,
            ),
          ),
          const SizedBox(height: 6),
          WidgetTourDetailInfo(
            title: "05/02/2021 - C0000000001 - Chuyến tham quan",
            onTapInfo: () => Navigator.pushNamed(
              context,
              Routers.tour_more_detail,
              arguments: widget.tourDetail,
            ),
          ),
          const SizedBox(height: 6),
          WidgetTourDetailInfo(
            title: "05/02/2021 - C0000000001 - Chuyến tham quan",
            onTapInfo: () => Navigator.pushNamed(
              context,
              Routers.tour_more_detail,
              arguments: widget.tourDetail,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTitle({String title, String content}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.primaryDark,
            fontSize: 17,
          ),
        ),
        Text(
          content,
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            fontSize: 16.5,
          ),
          textAlign: TextAlign.end,
        ),
      ],
    );
  }

  Widget _buildTour() {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      color: Colors.white,
      elevation: 3,
      child: Padding(
        padding: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              _viewModel.model.tour.name,
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                fontSize: 18,
                color: AppColors.primaryDark,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 8),
            Wrap(
              spacing: 5,
              children: List.generate(
                _viewModel.model.tour.star,
                (index) => Icon(
                  Icons.star,
                  size: 15,
                  color: AppColors.orangeLight,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTabBar({List<Widget> tabs}) {
    return TabBar(
      controller: _tabController,
      labelStyle: AppStyles.DEFAULT_MEDIUM,
      indicator: UnderlineTabIndicator(
        borderSide: BorderSide(
          width: 2,
          color: AppColors.red,
        ),
        insets: EdgeInsets.symmetric(horizontal: 45),
      ),
      isScrollable: false,
      physics: NeverScrollableScrollPhysics(),
      onTap: (index) => _viewModel.setSelected(index),
      labelPadding: EdgeInsets.only(top: 15),
      tabs: tabs,
    );
  }

  Widget _buildTab({String title, int index}) {
    return Container(
      width: AppSize.screenWidth / 3 - 40,
      padding: EdgeInsets.only(bottom: 5),
      child: Text(
        title,
        style: AppStyles.DEFAULT_SMALL_BOLD.copyWith(
          color: _viewModel.tabSelected == index ? AppColors.black : AppColors.grey,
          fontSize: 13,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildTextField({TextEditingController controller, String hintText}) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(Colors.transparent),
        contentPadding: EdgeInsets.fromLTRB(15.0, 12.0, 15.0, 12.0),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  Widget _buildRowInfo({String header, String content, Widget child}) {
    return Container(
      width: AppSize.screenWidth,
      height: 50,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                header,
                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: AppColors.grey,
                ),
              ),
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                content ?? "",
                style: AppStyles.DEFAULT_MEDIUM,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          child ?? Container(),
        ],
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(5),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';

import '../../src.dart';

class TourDetailViewModel extends BaseViewModel {
  TextEditingController nameTourController = TextEditingController();
  TextEditingController timeStartController = TextEditingController();
  TextEditingController timeEndController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController moneyController = TextEditingController();

  List<ChairModel> chair1 = [];
  List<ChairModel> chair2 = [];
  TourDetailModel model;

  bool addTour = false;
  int imgSelected = 0;
  int tabSelected = 0;

  TourDetailViewModel({this.model});

  init() {
    chair1 = [
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
    ];
    chair2 = [
      ChairModel(name: "A2", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
    ];
  }

  setSelected(int index) {
    tabSelected = index;
    model.tour = model.tours[index];
    notifyListeners();
  }

  switchAddTour() {
    addTour = !addTour;
    notifyListeners();
  }
}

import 'package:animated_icon_button/animated_icon_button.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetTourDetailInfo extends StatefulWidget {
  final String title;
  final Function onTapInfo;

  WidgetTourDetailInfo({this.title, this.onTapInfo});

  @override
  _WidgetTourDetailInfoState createState() => _WidgetTourDetailInfoState();
}

class _WidgetTourDetailInfoState extends State<WidgetTourDetailInfo> with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  bool _isOpen = false;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(duration: Duration(milliseconds: 250), vsync: this,);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () => _handleShowItem(),
          child: Container(
            width: AppSize.screenWidth - 30,
            height: 50,
            padding: EdgeInsets.only(left: 12, right: 10),
            decoration: BoxDecoration(
              color: AppColors.greyLight,
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    widget.title,
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: AppColors.primaryDark,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                AnimatedIconButton(
                  animationController: _animationController,
                  size: 28,
                  onPressed: () {},
                  endIcon: Icon(
                    Icons.arrow_drop_down,
                    color: AppColors.primaryDark,
                  ),
                  startIcon: Icon(
                    Icons.arrow_drop_up,
                    color: AppColors.primaryDark,
                  ),
                ),
              ],
            ),
          ),
        ),
        if (_isOpen)
          Column(
            children: [
              const SizedBox(height: 8),
              _buildTitle(
                title: "Ngày khởi hành:",
                content: "05-02-2021",
                titleColor: AppColors.black,
                contentColor: AppColors.grey,
              ),
              const SizedBox(height: 8),
              _buildTitle(
                title: "Giờ khởi hành:",
                content: "16:00",
                titleColor: AppColors.black,
                contentColor: AppColors.grey,
              ),
              const SizedBox(height: 8),
              _buildTitle(
                title: "Giờ khởi thúc:",
                content: "18:00",
                titleColor: AppColors.black,
                contentColor: AppColors.grey,
              ),
              const SizedBox(height: 8),
              _buildTitle(
                title: "Mã số tàu (Tên):",
                content: "B001 -\nSaigon Water taxi",
                titleColor: AppColors.black,
                contentColor: AppColors.grey,
              ),
              const SizedBox(height: 8),
              _buildTitle(
                title: "Địa điểm xuất bến:",
                content: "Bến Thuỷ -\nBạch Đằng",
                titleColor: AppColors.black,
                contentColor: AppColors.grey,
              ),
              const SizedBox(height: 8),
              _buildTitle(
                title: "Người hướng dẫn:",
                content: "Nguyễn Thị Huyền",
                titleColor: AppColors.black,
                contentColor: AppColors.grey,
              ),
              const SizedBox(height: 8),
              _buildTitle(
                title: "Trạng thái:",
                content: "Close (Full)",
                titleColor: AppColors.black,
                contentColor: AppColors.red,
              ),
              const SizedBox(height: 8),
              _buildTitle(
                title: "Phí (giá vé):",
                content: "2.500.000 VND",
                titleColor: AppColors.black,
                contentColor: AppColors.primaryDark,
              ),
              const SizedBox(height: 17),
              GestureDetector(
                onTap: widget.onTapInfo ?? () {},
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.search,
                      color: AppColors.red,
                    ),
                    const SizedBox(width: 8),
                    Text(
                      "Chi tiết thông tin đặt chỗ",
                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                        color: AppColors.red,
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 12),
            ],
          ),
      ],
    );
  }

  _handleShowItem() {
    setState(() {
      _isOpen = !_isOpen;
      _isOpen ? _animationController.forward() : _animationController.reverse();
    });
  }

  Widget _buildTitle({
    String title,
    String content,
    Color titleColor,
    Color contentColor,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: titleColor ?? AppColors.primaryDark,
            fontSize: 17,
          ),
        ),
        Text(
          content,
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            fontSize: 16.5,
            color: contentColor ?? AppColors.black,
          ),
          textAlign: TextAlign.end,
        ),
      ],
    );
  }
}

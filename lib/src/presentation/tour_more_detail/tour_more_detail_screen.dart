import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class TourMoreDetailScreen extends StatefulWidget {
  final TourDetailModel tourDetail;

  TourMoreDetailScreen({this.tourDetail});

  @override
  _TourMoreDetailScreenState createState() => _TourMoreDetailScreenState();
}

class _TourMoreDetailScreenState extends State<TourMoreDetailScreen> with SingleTickerProviderStateMixin {
  TourMoreDetailViewModel _viewModel;
  TabController _tabController;
  int _tabs;

  @override
  void initState() {
    super.initState();
    _tabs = widget.tourDetail.user.getTabs();
    _tabController = TabController(length: _tabs, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<TourMoreDetailViewModel>(
      viewModel: TourMoreDetailViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: _buildBody(),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    List<String> images = [];
    for (var x in widget.tourDetail.tours) images.add(x.imageUrl);

    return SingleChildScrollView(
      child: Stack(
        children: [
          Column(
            children: [
              Stack(
                overflow: Overflow.visible,
                children: [
                  WidgetCarousel(
                    images: images,
                    circular: 0,
                    onPageChanged: _viewModel.setSelected,
                  ),
                  Positioned(
                    left: 12,
                    right: 12,
                    bottom: -40,
                    child: _buildTour(),
                  ),
                ],
              ),
              const SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 12, right: 12, bottom: 12),
                child: _buildMainTour(),
              ),
              if (_viewModel.tabSelected == 1 && _tabs > 2)
                Padding(
                  padding: const EdgeInsets.only(left: 12, right: 12, bottom: 12),
                  child: WidgetButtonGradientAni(
                    width: AppSize.screenWidth,
                    title: "Đặt chỗ",
                    trans: false,
                    textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: AppColors.white,
                    ),
                    colorStart: AppColors.primaryDark,
                    colorEnd: AppColors.primaryDark,
                    action: () => Navigator.pushNamed(context, Routers.reservation_info),
                  ),
                ),
            ],
          ),
          WidgetButtonBack(
            color: Colors.white,
            size: 18,
          ),
        ],
      ),
    );
  }

  //region Build Tour Customer
  Widget _buildTourCustomer() {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      color: Colors.white,
      elevation: 3,
      child: Column(
        children: [
          _buildTabBar(
            tabs: [
              _buildTab(title: "Thông tin chuyến", index: 0),
              _buildTab(title: "Thông tin đặt chỗ", index: 1),
            ],
          ),
          Container(
            width: AppSize.screenWidth,
            height: AppSize.screenHeight / 2,
            child: TabBarView(
              controller: _tabController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                _buildTourCustomerView1(),
                _buildTourCustomerView2(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTourCustomerView1() {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          const SizedBox(height: 8),
          _buildTitle(
            title: "Ngày khởi hành:",
            content: "05-02-2021",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Giờ khởi hành:",
            content: "16:00",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Giờ khởi thúc:",
            content: "18:00",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Mã số tàu (Tên):",
            content: "B001 -\nSaigon Water taxi",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Địa điểm xuất bến:",
            content: "Bến Thuỷ -\nBạch Đằng",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Người hướng dẫn:",
            content: "Nguyễn Thị Huyền",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Trạng thái:",
            content: "Close (Full)",
            titleColor: AppColors.black,
            contentColor: AppColors.red,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Phí (giá vé):",
            content: "2.500.000 VND",
            titleColor: AppColors.black,
            contentColor: AppColors.primaryDark,
          ),
        ],
      ),
    );
  }

  Widget _buildTourCustomerView2() {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildTitle(
            title: "Tổng số chỗ",
            content: "16",
            titleColor: AppColors.black,
            contentColor: AppColors.black,
            isBold: true,
          ),
          const SizedBox(height: 6),
          _buildTitle(
            title: "Số chỗ đã xác nhận",
            content: "8",
            titleColor: AppColors.black,
            contentColor: AppColors.primaryDark,
          ),
          const SizedBox(height: 6),
          _buildTitle(
            title: "Số chỗ đang chờ xác nhận",
            content: "3",
            titleColor: AppColors.black,
            contentColor: AppColors.red,
          ),
          const SizedBox(height: 6),
          _buildTitle(
            title: "Số chỗ còn trống",
            content: "5",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildChair(chairs: _viewModel.chair1),
              _buildChair(chairs: _viewModel.chair2),
            ],
          ),
        ],
      ),
    );
  }

  //endregion

  //region Build Tour Moderator
  Widget _buildTourModerator() {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      color: Colors.white,
      elevation: 3,
      child: Column(
        children: [
          _buildTabBar(
            tabs: [
              _buildTab(title: "Thông tin\nchuyến", index: 0),
              _buildTab(title: "Thông tin\nđặt chỗ", index: 1),
              _buildTab(title: "Danh sách\nkhách", index: 2),
            ],
          ),
          Container(
            width: AppSize.screenWidth,
            height: AppSize.screenHeight / 2,
            child: TabBarView(
              controller: _tabController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                _buildTourModeratorView1(),
                _buildTourModeratorView2(),
                _buildTourModeratorView3(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTourModeratorView1() {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          const SizedBox(height: 8),
          _buildTitle(
            title: "Ngày khởi hành:",
            content: "05-02-2021",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Giờ khởi hành:",
            content: "16:00",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Giờ khởi thúc:",
            content: "18:00",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Mã số tàu (Tên):",
            content: "B001 -\nSaigon Water taxi",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Địa điểm xuất bến:",
            content: "Bến Thuỷ -\nBạch Đằng",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Người hướng dẫn:",
            content: "Nguyễn Thị Huyền",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Trạng thái:",
            content: "Close (Full)",
            titleColor: AppColors.black,
            contentColor: AppColors.red,
          ),
          const SizedBox(height: 8),
          _buildTitle(
            title: "Phí (giá vé):",
            content: "2.500.000 VND",
            titleColor: AppColors.black,
            contentColor: AppColors.primaryDark,
          ),
        ],
      ),
    );
  }

  Widget _buildTourModeratorView2() {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildTitle(
            title: "Tổng số chỗ",
            content: "16",
            titleColor: AppColors.black,
            contentColor: AppColors.black,
            isBold: true,
          ),
          const SizedBox(height: 6),
          _buildTitle(
            title: "Số chỗ đã xác nhận",
            content: "8",
            titleColor: AppColors.black,
            contentColor: AppColors.primaryDark,
          ),
          const SizedBox(height: 6),
          _buildTitle(
            title: "Số chỗ đang chờ xác nhận",
            content: "3",
            titleColor: AppColors.black,
            contentColor: AppColors.red,
          ),
          const SizedBox(height: 6),
          _buildTitle(
            title: "Số chỗ còn trống",
            content: "5",
            titleColor: AppColors.black,
            contentColor: AppColors.grey,
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildChair(chairs: _viewModel.chair1),
              _buildChair(chairs: _viewModel.chair2),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildTourModeratorView3({bool showAcceptButton = false}) {
    return ListView.separated(
      padding: EdgeInsets.fromLTRB(20, 15, 15, 15),
      itemBuilder: (context, index) => _buildCustomer(
        _viewModel.customers[index],
        showAcceptButton,
      ),
      separatorBuilder: (context, index) => Container(height: 10),
      itemCount: _viewModel.customers.length,
    );
  }

  //endregion

  //region Build Tour Admin
  Widget _buildTourAdmin() {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      color: Colors.white,
      elevation: 3,
      child: Column(
        children: [
          _buildTabBar(
            tabs: [
              _buildTab(title: "Thông tin\nchuyến", index: 0),
              _buildTab(title: "Thông tin\nđặt chỗ", index: 1),
              _buildTab(title: "Danh sách\nkhách", index: 2),
              _buildTab(title: "Danh sách\ncần duyệt", index: 3),
            ],
          ),
          Container(
            width: AppSize.screenWidth,
            height: AppSize.screenHeight / 2,
            child: TabBarView(
              controller: _tabController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                _buildTourModeratorView1(),
                _buildTourModeratorView2(),
                _buildTourModeratorView3(),
                _buildTourModeratorView3(showAcceptButton: true),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //endregion

  //region Base Build
  Widget _buildCustomer(CustomerModel model, bool showAcceptButton) {
    return Stack(
      overflow: Overflow.visible,
      alignment: Alignment.center,
      children: [
        GestureDetector(
          onTap: () => widget.tourDetail.user.privilege == Privilege.Admin && model.isAccept
              ? Navigator.pushNamed(context, Routers.customer_info)
              : {},
          child: DottedBorder(
            borderType: BorderType.RRect,
            color: model.isAccept ? AppColors.primaryDark : Colors.transparent,
            radiusTopLeft: Radius.circular(5),
            radiusTopRight: Radius.circular(5),
            radiusBottomRight: Radius.circular(5),
            radiusBottomLeft: Radius.circular(5),
            dashPattern: [7, 5],
            child: Container(
              width: AppSize.screenWidth,
              height: 60,
              decoration: BoxDecoration(
                color: model.isAccept ? Colors.white : AppColors.white3.withOpacity(0.5),
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              padding: EdgeInsets.only(left: 20, top: 7, right: 7, bottom: 7),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    model.name,
                    style: AppStyles.DEFAULT_MEDIUM,
                  ),
                  const SizedBox(height: 6),
                  if (model.isAccept)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Đã xác nhận",
                          style: AppStyles.DEFAULT_MEDIUM.copyWith(
                            color: AppColors.primaryDark,
                          ),
                        ),
                        Text(
                          model.code,
                          style: AppStyles.DEFAULT_MEDIUM,
                        ),
                      ],
                    )
                  else
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Chờ xác nhận",
                          style: AppStyles.DEFAULT_MEDIUM.copyWith(
                            color: AppColors.red,
                          ),
                        ),
                        if (showAcceptButton)
                          GestureDetector(
                            onTap: () => _viewModel.handleConfirm(msg: "Xác nhận thông tin khách hàng?"),
                            child: Text(
                              "Xác nhận",
                              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                color: AppColors.primaryDark,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                      ],
                    ),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          left: -12.5,
          child: Container(
            width: 25,
            height: 25,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.5,
                color: AppColors.white,
              ),
              shape: BoxShape.circle,
              color: AppColors.primaryDark,
            ),
            child: Center(
              child: Text(
                model.index.toString(),
                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildMainTour() {
    print(widget.tourDetail.user.privilege);
    switch (widget.tourDetail.user.privilege) {
      case Privilege.Admin:
        return _buildTourAdmin();
      case Privilege.Moderator:
        return _buildTourModerator();
      case Privilege.Customer:
        return _buildTourCustomer();
    }
  }

  Widget _buildTour() {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      color: Colors.white,
      elevation: 3,
      child: Padding(
        padding: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Tham quan đảo Angel Island bằng du thuyền",
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                fontSize: 18,
                color: AppColors.primaryDark,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 8),
            Wrap(
              spacing: 5,
              children: List.generate(
                5,
                (index) => Icon(
                  Icons.star,
                  size: 15,
                  color: AppColors.orangeLight,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildChair({List<ChairModel> chairs}) {
    double padding = AppSize.screenWidth / 2 - 150;
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: padding, right: padding),
        child: Wrap(
          spacing: 5,
          runSpacing: 5,
          children: List.generate(
            chairs.length,
            (index) => Stack(
              alignment: Alignment.center,
              children: [
                Icon(
                  Icons.event_seat,
                  size: 30,
                  color: chairs[index].chairColor(),
                ),
                Text(
                  chairs[index].name,
                  style: AppStyles.DEFAULT_SMALL.copyWith(
                    color: AppColors.black,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle({
    String title,
    String content,
    Color titleColor,
    Color contentColor,
    bool isBold = false,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: isBold
              ? AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                  color: titleColor,
                  fontSize: 17,
                )
              : AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: titleColor,
                  fontSize: 17,
                ),
        ),
        Text(
          content,
          style: isBold
              ? AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                  color: contentColor,
                  fontSize: 16.5,
                )
              : AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: contentColor,
                  fontSize: 16.5,
                ),
          textAlign: TextAlign.end,
        ),
      ],
    );
  }

  Widget _buildTabBar({List<Widget> tabs}) {
    return TabBar(
      controller: _tabController,
      labelStyle: AppStyles.DEFAULT_MEDIUM,
      indicator: UnderlineTabIndicator(
        borderSide: BorderSide(
          width: 2,
          color: AppColors.red,
        ),
        insets: EdgeInsets.symmetric(horizontal: (_tabs + 1) * 10.0),
      ),
      isScrollable: false,
      physics: NeverScrollableScrollPhysics(),
      onTap: (index) => _viewModel.setSelected(index),
      labelPadding: EdgeInsets.only(top: 15),
      tabs: tabs,
    );
  }

  Widget _buildTab({String title, int index}) {
    return Container(
      width: AppSize.screenWidth / _tabs - 40,
      padding: EdgeInsets.only(bottom: 10),
      child: Text(
        title,
        style: AppStyles.DEFAULT_SMALL_BOLD.copyWith(
          color: _viewModel.tabSelected == index ? AppColors.black : AppColors.grey,
          fontSize: 14,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
//endregion
}

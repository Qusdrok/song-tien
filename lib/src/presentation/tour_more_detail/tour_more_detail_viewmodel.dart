import '../../src.dart';

class TourMoreDetailViewModel extends BaseViewModel {
  List<CustomerModel> customers = [];
  List<ChairModel> chair1 = [];
  List<ChairModel> chair2 = [];

  int imgSelected = 0;
  int tabSelected = 0;

  init() {
    chair1 = [
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A1", status: ChairStatus.Confirmation),
      ChairModel(name: "A1", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A1", status: ChairStatus.Other),
    ];
    chair2 = [
      ChairModel(name: "A2", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
      ChairModel(name: "A1", status: ChairStatus.Empty),
      ChairModel(name: "A2", status: ChairStatus.Confirmation),
      ChairModel(name: "A2", status: ChairStatus.WaitingConfirmation),
      ChairModel(name: "A2", status: ChairStatus.Other),
    ];
    customers = [
      CustomerModel(
        code: "ABC0123",
        isAccept: false,
        name: "Trần Phương Anh",
        index: 1,
      ),
      CustomerModel(
        code: "ABC0123",
        isAccept: true,
        name: "Trần Phương Anh",
        index: 2,
      ),
      CustomerModel(
        code: "ABC0123",
        isAccept: false,
        name: "Trần Phương Anh",
        index: 3,
      ),
      CustomerModel(
        code: "ABC0123",
        isAccept: true,
        name: "Trần Phương Anh",
        index: 4,
      ),
      CustomerModel(
        code: "ABC0123",
        isAccept: false,
        name: "Trần Phương Anh",
        index: 5,
      ),
      CustomerModel(
        code: "ABC0123",
        isAccept: true,
        name: "Trần Phương Anh",
        index: 6,
      ),
      CustomerModel(
        code: "ABC0123",
        isAccept: false,
        name: "Trần Phương Anh",
        index: 7,
      ),
      CustomerModel(
        code: "ABC0123",
        isAccept: true,
        name: "Trần Phương Anh",
        index: 11,
      ),
    ];
  }

  setSelected(int index) {
    tabSelected = index;
    notifyListeners();
  }

  Future handleConfirm({String msg}) async {
    print("click");
    await showRequestConfirm(
      content: msg,
      transTitle: false,
      actionConfirm: () {},
    );
  }
}

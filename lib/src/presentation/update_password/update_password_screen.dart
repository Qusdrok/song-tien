import 'package:flutter/material.dart';

import '../../src.dart';

class UpdatePasswordScreen extends StatefulWidget {
  @override
  _UpdatePasswordScreenState createState() => _UpdatePasswordScreenState();
}

class _UpdatePasswordScreenState extends State<UpdatePasswordScreen> {
  UpdatePasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<UpdatePasswordViewModel>(
      viewModel: UpdatePasswordViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: SafeArea(
              child: Column(
                children: [
                  _buildHeader(),
                  _buildBody(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildHeader() {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(60),
          ),
          child: Image.asset(
            AppImages.imgBgLogin,
            width: AppSize.screenWidth,
            height: 210,
            fit: BoxFit.fill,
          ),
        ),
        const SizedBox(height: 10),
        WidgetLogo(),
      ],
    );
  }

  Widget _buildBody() {
    return Container(
      width: AppSize.screenWidth,
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
      color: Colors.transparent,
      child: Column(
        children: [
          Container(
            width: AppSize.screenWidth,
            height: 110,
            alignment: Alignment.center,
            padding: EdgeInsets.only(top: 10),
            child: Text(
              AppLocalizations.of(context).translate("change_password").toUpperCase(),
              style: AppStyles.DEFAULT_LARGE.copyWith(
                color: AppColors.primary,
                fontSize: 24,
              ),
            ),
          ),
          Form(
            key: _viewModel.formUpdatePassword,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                _buildTextField(
                  validator: AppValid.validatePassword(context),
                  controller: _viewModel.passwordController,
                  hintText: "new_password",
                  inputType: TextInputType.emailAddress,
                ),
                SizedBox(height: 10),
                _buildTextField(
                  validator: AppValid.validatePassword(context),
                  controller: _viewModel.confirmPasswordController,
                  hintText: "new_confirm_password",
                  inputType: TextInputType.emailAddress,
                ),
                SizedBox(height: 25),
                WidgetButtonGradientAni(
                  width: AppSize.screenWidth - 15,
                  title: "change",
                  colorStart: AppColors.primaryDark,
                  colorEnd: AppColors.primaryDark,
                  textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.white,
                  ),
                  action: () {},
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTextField({
    TextEditingController controller,
    String hintText,
    TextInputType inputType,
    validator,
    bool isPassword = false,
    bool readOnly = false,
  }) {
    return TextFormField(
      controller: controller,
      readOnly: readOnly,
      obscureText: _viewModel.obSecureText,
      validator: validator,
      keyboardType: inputType ?? TextInputType.text,
      decoration: InputDecoration(
        suffixIcon: Padding(
          padding: const EdgeInsets.only(right: 12),
          child: IconButton(
            icon: Icon(
              _viewModel.obSecureText ? Icons.remove_red_eye_outlined : Icons.remove_red_eye,
              color: AppColors.grey,
            ),
            color: Colors.black,
            onPressed: () => _viewModel.showPassword(),
          ),
        ),
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(AppColors.primaryLight),
        errorBorder: _buildBorderTextField(AppColors.red),
        focusedErrorBorder: _buildBorderTextField(AppColors.primaryLight),
        contentPadding: EdgeInsets.fromLTRB(25.0, 16.0, 25.0, 16.0),
        filled: true,
        fillColor: Colors.white,
        hintText: AppLocalizations.of(context).translate(hintText),
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(150),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../../src.dart';

class UpdatePasswordViewModel extends BaseViewModel {
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  GlobalKey<FormState> formUpdatePassword = GlobalKey<FormState>();

  bool obSecureText = true;

  init() {}

  showPassword() {
    obSecureText = !obSecureText;
    notifyListeners();
  }
}

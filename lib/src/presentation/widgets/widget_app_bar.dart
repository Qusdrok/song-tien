import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetAppBar extends StatelessWidget {
  final Color colorButton;
  final Color colorTitle;
  final double sizeLeading;
  final double height;
  final String keyTitle;
  final Function actionBack;
  final List<Widget> actions;
  final Widget children;
  final bool trans;
  final bool isBack;

  const WidgetAppBar({
    this.height,
    this.colorButton = Colors.black,
    this.colorTitle = Colors.white,
    this.sizeLeading = 18,
    this.keyTitle,
    this.trans = true,
    this.isBack = true,
    this.children,
    this.actionBack,
    this.actions,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: AppSize.screenWidth,
        height: AppValues.HEIGHT_APP_BAR + (height ?? 0),
        color: AppColors.primaryDark,
        child: Column(
          children: [
            Row(
              children: [
                WidgetButtonBack(
                  action: actionBack,
                  size: sizeLeading,
                  color: colorButton,
                  isBack: isBack,
                ),
                Expanded(
                  child: keyTitle == null
                      ? const SizedBox()
                      : Center(
                          child: Text(
                            trans ? AppLocalizations.of(context).translate(keyTitle) : keyTitle,
                            style: AppStyles.DEFAULT_LARGE.copyWith(
                              color: colorTitle ?? Colors.white,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                ),
                actions == null
                    ? Opacity(
                        opacity: 0,
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: colorButton,
                            size: sizeLeading,
                          ),
                        ),
                      )
                    : Row(children: actions),
              ],
            ),
            if (children != null) children,
          ],
        ),
      ),
    );
  }
}

class WidgetButtonBack extends StatelessWidget {
  final Color color;
  final double size;
  final action;
  final bool isBack;

  const WidgetButtonBack({
    this.color,
    this.size,
    this.action,
    this.isBack = true,
  });

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: isBack ? 1 : 0,
      child: IconButton(
        icon: Icon(
          Icons.arrow_back_ios_rounded,
          color: color ?? Colors.grey,
          size: size ?? 18,
        ),
        onPressed: isBack ? action ?? () => Navigator.pop(context) : null,
      ),
    );
  }
}

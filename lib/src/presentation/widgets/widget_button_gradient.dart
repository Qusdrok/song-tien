import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetButtonGradient extends StatelessWidget {
  final bool loading;
  final String title;
  final Function action;
  final Color colorStart;
  final Color colorEnd;
  final Color colorLoading;
  final Color colorBorder;
  final Color colorTitle;
  final Alignment alignmentStart;
  final Alignment alignmentEnd;
  final EdgeInsets padding;
  final double width;

  const WidgetButtonGradient({
    this.alignmentStart,
    this.alignmentEnd,
    this.colorLoading,
    this.colorBorder,
    this.colorEnd,
    this.colorStart,
    this.loading = false,
    this.padding,
    this.colorTitle,
    @required this.width,
    @required this.title,
    @required this.action,
  });

  static const double HEIGHT = 40.0;
  static const double WIDTH = 40.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: HEIGHT,
      width: width ?? WIDTH,
      child: RaisedButton(
        onPressed: loading ? null : action ?? () {},
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: const EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                colorStart ?? AppColors.primary,
                colorEnd ?? AppColors.primaryDark,
              ],
              begin: alignmentStart ?? Alignment.bottomCenter,
              end: alignmentEnd ?? Alignment.topCenter,
            ),
            borderRadius: BorderRadius.circular(30.0),
            border: Border.all(
              color: colorBorder ?? Colors.transparent,
              width: 1,
            ),
          ),
          child: Container(
            padding: padding ?? const EdgeInsets.all(0.0),
            alignment: Alignment.center,
            child: !loading
                ? Text(
                    title,
                    textAlign: TextAlign.center,
                    style: AppStyles.DEFAULT_SMALL.copyWith(
                      color: colorTitle ?? Colors.white,
                      fontSize: 14,
                    ),
                  )
                : Center(
                    child: WidgetLoading(
                      dotOneColor: colorLoading ?? Colors.white,
                      dotTwoColor: colorLoading ?? Colors.white,
                      dotThreeColor: colorLoading ?? Colors.white,
                      dotType: DotType.circle,
                      duration: Duration(milliseconds: 1000),
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}

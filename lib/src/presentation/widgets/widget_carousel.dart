import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetCarousel extends StatefulWidget {
  final List<String> images;
  final double widthImage;
  final double heightImage;
  final BoxFit fitImage;
  final double bottomPaddingDot;
  final double circular;
  final Function(int index) onPageChanged;

  WidgetCarousel({
    this.images,
    this.widthImage,
    this.heightImage,
    this.circular = 5,
    this.fitImage,
    this.bottomPaddingDot,
    this.onPageChanged,
  });

  @override
  _WidgetCarouselState createState() => _WidgetCarouselState();
}

class _WidgetCarouselState extends State<WidgetCarousel> {
  int _tabSelected = 0;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        CarouselSlider(
          items: List.generate(
            widget.images.length,
            (index) => ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(widget.circular)),
              child: Image.asset(
                widget.images[index],
                width: widget.widthImage ?? AppSize.screenWidth,
                fit: widget.fitImage ?? BoxFit.fill,
              ),
            ),
          ),
          options: CarouselOptions(
            height: widget.heightImage ?? 300,
            viewportFraction: 1,
            enableInfiniteScroll: true,
            initialPage: 1,
            aspectRatio: 2,
            autoPlay: false,
            enlargeCenterPage: false,
            onPageChanged: (index, _) {
              _tabSelected = index;
              widget.onPageChanged(index);
            },
          ),
        ),
        Positioned(
          bottom: widget.bottomPaddingDot ?? 52,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(
              widget.images.length,
              (index) => _buildDot(index),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDot(int index) {
    bool isChange = _tabSelected == index;
    return AnimatedContainer(
      duration: Duration(milliseconds: 350),
      width: isChange ? 14 : 8,
      height: 8,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 2),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: Colors.white,
        ),
        borderRadius: BorderRadius.all(Radius.circular(15)),
        color: isChange ? AppColors.primary : AppColors.white,
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetCircleProgress extends StatelessWidget {
  final Color color;
  final Color colorBackground;

  const WidgetCircleProgress({this.color, this.colorBackground});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      height: 40,
      padding: const EdgeInsets.all(5),
      child: CircularProgressIndicator(
        strokeWidth: 4.5,
        valueColor: AlwaysStoppedAnimation(color ?? AppColors.primary),
        backgroundColor: colorBackground ?? AppColors.primaryLight,
      ),
    );
  }
}

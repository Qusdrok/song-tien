import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../configs/configs.dart';
import '../presentation.dart';

typedef Future<List> DataRequesterWrap(int offset);
typedef Future<List> InitRequesterWrap();
typedef Widget ItemBuilderWrap(List data, BuildContext context, int index);
typedef Widget ItemBuilderHeader(List data, BuildContext context);

class WidgetLoadMoreWrapVertical<T> extends StatefulWidget {
  WidgetLoadMoreWrapVertical.build(
      {Key key,
      @required this.itemBuilder,
      @required this.dataRequester,
      @required this.initRequester,
      this.padding,
      this.styleError,
      this.loadingColor,
      this.loadingColorBackground,
      this.widgetError})
      : assert(itemBuilder != null),
        assert(dataRequester != null),
        assert(initRequester != null),
        super(key: key);

  final TextStyle styleError;
  final ItemBuilderWrap itemBuilder;
  final DataRequesterWrap dataRequester;
  final InitRequesterWrap initRequester;
  final EdgeInsets padding;
  final Color loadingColor;
  final Color loadingColorBackground;
  final Widget widgetError;

  @override
  State createState() => new WidgetLoadMoreWrapVerticalState<T>();
}

class WidgetLoadMoreWrapVerticalState<T>
    extends State<WidgetLoadMoreWrapVertical> {
  bool isPerformingRequest = false;
  ScrollController _controller = new ScrollController();
  List<T> _dataList;
  List<Widget> children;

  @override
  void initState() {
    super.initState();
    this.onRefresh();
    _controller.addListener(() {
      if (_controller.position.pixels == _controller.position.maxScrollExtent) {
        _loadMore();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    children = _dataList
        ?.map(
            (e) => widget.itemBuilder(_dataList, context, _dataList.indexOf(e)))
        ?.toList();
    return this._dataList == null
        ? loadingProgress()
        : (this._dataList.length > 0
            ? RefreshIndicator(
                color: widget.loadingColor ?? AppColors.primary,
                onRefresh: this.onRefresh,
                child: SingleChildScrollView(
                  controller: _controller,
                  padding: widget.padding ?? EdgeInsets.zero,
                  child: Wrap(
                    children: children +
                        [opacityLoadingProgress(isPerformingRequest)],
                  ),
                ),
              )
            : RefreshIndicator(
                color: widget.loadingColor ?? AppColors.primaryLight,
                onRefresh: this.onRefresh,
                child: Stack(
                  children: [
                    ListView(),
                    Center(
                        child: widget.widgetError ??
                            Text(
                              "Không có dữ liệu",
                            )),
                  ],
                ),
              ));
  }

  Future<Null> onRefresh() async {
    if (mounted) this.setState(() => this._dataList = null);
    List initDataList = await widget.initRequester();
    if (mounted) this.setState(() => this._dataList = initDataList);
    return;
  }

  _loadMore() async {
    if (mounted) {
      this.setState(() => isPerformingRequest = true);
      int currentSize = 0;
      if (_dataList != null) currentSize = _dataList.length;

      List<T> newDataList = await widget.dataRequester(currentSize);
      if (newDataList != null) {
        if (newDataList.length == 0) {
          double edge = 50.0;
          double offsetFromBottom = _controller.position.maxScrollExtent -
              _controller.position.pixels;
          if (offsetFromBottom < edge) {
            _controller.animateTo(
                _controller.offset - (edge - offsetFromBottom),
                duration: new Duration(milliseconds: 500),
                curve: Curves.easeOut);
          }
        } else {
          _dataList.addAll(newDataList);
        }
      }
      if (mounted) this.setState(() => isPerformingRequest = false);
    }
  }

  Widget loadingProgress() {
    return Center(
      child: WidgetCircleProgress(
        color: widget.loadingColor,
        colorBackground: widget.loadingColorBackground,
      ),
    );
  }

  Widget opacityLoadingProgress(isPerformingRequest) {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isPerformingRequest ? 1.0 : 0.0,
          child: WidgetCircleProgress(
            color: widget.loadingColor,
            colorBackground: widget.loadingColorBackground,
          ),
        ),
      ),
    );
  }
}

class WidgetLoadMoreWrapHeader<T> extends StatefulWidget {
  WidgetLoadMoreWrapHeader.build(
      {Key key,
      @required this.itemBuilder,
      @required this.dataRequester,
      @required this.initRequester,
      this.widgetHeader,
      this.rangeHeader = 3,
      this.padding,
      this.styleError,
      this.loadingColor,
      this.loadingColorBackground,
      this.widgetError})
      : assert(itemBuilder != null),
        assert(dataRequester != null),
        assert(initRequester != null),
        super(key: key);

  final TextStyle styleError;
  final ItemBuilderWrap itemBuilder;
  final DataRequesterWrap dataRequester;
  final InitRequesterWrap initRequester;
  final EdgeInsets padding;
  final Color loadingColor;
  final Color loadingColorBackground;
  final Widget widgetError;
  final ItemBuilderHeader widgetHeader;
  final int rangeHeader;

  @override
  State createState() => new WidgetLoadMoreWrapVerticalState2<T>();
}

class WidgetLoadMoreWrapVerticalState2<T>
    extends State<WidgetLoadMoreWrapHeader> {
  bool isPerformingRequest = false;
  ScrollController _controller = new ScrollController();
  List<T> _dataList;
  List<Widget> children;

  @override
  void initState() {
    super.initState();
    this.onRefresh();
    _controller.addListener(() {
      if (_controller.position.pixels == _controller.position.maxScrollExtent &&
          this._dataList != null) {
        _loadMore();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget widgetHeader;
    if (_dataList != null && widget.widgetHeader != null)
      widgetHeader = widget.widgetHeader(_dataList, context);
    int range = 0;
    if (_dataList != null && _dataList.length - 1 < widget.rangeHeader)
      range = _dataList.length - 1;
    else if (_dataList != null && _dataList.length >= widget.rangeHeader)
      range = widget.rangeHeader;
    children = ((_dataList?.length == 0)
            ? _dataList
            : _dataList?.getRange(range, _dataList.length - 1))
        ?.map(
            (e) => widget.itemBuilder(_dataList, context, _dataList.indexOf(e)))
        ?.toList();
    return this._dataList == null
        ? loadingProgress()
        : (this._dataList.length > 0
            ? RefreshIndicator(
                color: widget.loadingColor ?? AppColors.primary,
                onRefresh: this.onRefresh,
                child: SingleChildScrollView(
                  controller: _controller,
                  padding: widget.padding ?? EdgeInsets.zero,
                  child: Wrap(
                    children: [widgetHeader ?? SizedBox()] +
                        children +
                        [opacityLoadingProgress(isPerformingRequest)],
                  ),
                ),
              )
            : RefreshIndicator(
                color: widget.loadingColor ?? AppColors.primaryLight,
                onRefresh: this.onRefresh,
                child: Stack(
                  children: [
                    ListView(),
                    Center(
                        child: widget.widgetError ??
                            Text(
                              "Không có dữ liệu",
                            )),
                  ],
                ),
              ));
  }

  Future<Null> onRefresh() async {
    if (mounted) this.setState(() => this._dataList = null);
    List initDataList = await widget.initRequester();
    if (mounted) this.setState(() => this._dataList = initDataList);
    return;
  }

  _loadMore() async {
    try {
      if (mounted) {
        this.setState(() => isPerformingRequest = true);
        int currentSize = 0;
        if (_dataList != null) currentSize = _dataList.length;

        List<T> newDataList = await widget.dataRequester(currentSize);
        if (newDataList != null) {
          if (newDataList.length == 0) {
            double edge = 50.0;
            double offsetFromBottom = _controller.position.maxScrollExtent -
                _controller.position.pixels;
            if (offsetFromBottom < edge) {
              _controller.animateTo(
                  _controller.offset - (edge - offsetFromBottom),
                  duration: new Duration(milliseconds: 500),
                  curve: Curves.easeOut);
            }
          } else {
            _dataList.addAll(newDataList);
          }
        }
        if (mounted) this.setState(() => isPerformingRequest = false);
      }
    } catch (e) {}
  }

  Widget loadingProgress() {
    return WidgetShimmer(
        child: SingleChildScrollView(
      controller: _controller,
      padding: widget.padding ?? EdgeInsets.zero,
      child: Wrap(
          children: [widget.widgetHeader([], context)] +
              List.generate(
                  20,
                  (index) => widget.itemBuilder(
                      List.generate(20, (index) => null), context, index))),
    ));
    return Center(
      child: WidgetCircleProgress(
        color: widget.loadingColor,
        colorBackground: widget.loadingColorBackground,
      ),
    );
  }

  Widget opacityLoadingProgress(isPerformingRequest) {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isPerformingRequest ? 1.0 : 0.0,
          child: WidgetCircleProgress(
            color: widget.loadingColor,
            colorBackground: widget.loadingColorBackground,
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'widgets.dart';
import '../../configs/configs.dart';

class LoadingFullScreen extends StatelessWidget {
  final Widget child;
  final bool loading;

  const LoadingFullScreen({Key key, this.child, this.loading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => !loading,
      child: Container(
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: <Widget>[
            Container(
              constraints: BoxConstraints.expand(),
              child: child ?? Container(),
            ),
            loading == true ? _LoadingWidget() : Container(),
          ],
        ),
      ),
    );
  }
}

class _LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      alignment: Alignment.center,
      child: Container(
        width: 85,
        height: 85,
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(999)),
        child: Center(
          child: WidgetLoading(
            dotOneColor: AppColors.primary,
            dotTwoColor: AppColors.primary,
            dotThreeColor: AppColors.primary,
            dotType: DotType.circle,
            duration: Duration(milliseconds: 1000),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_app_song_tien/src/utils/app_size.dart';

class WidgetLoadingInStack extends StatelessWidget {
  final loadingSubject;
  final Widget child;

  const WidgetLoadingInStack({this.child, this.loadingSubject});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        child,
        StreamBuilder<bool>(
            stream: loadingSubject,
            builder: (context, snapshot) {
              return (snapshot.data ?? false)
                  ? Container(
                      width: AppSize.screenWidth,
                      height: AppSize.screenHeight,
                      color: Colors.black38,
                      child: Center(
                        child: Image.asset(
                          "assets/animation/loading_running.gif",
                          height: 95.0,
                          width: 95.0,
                        ),
                      ),
                    )
                  : SizedBox();
            }),
      ],
    );
  }
}

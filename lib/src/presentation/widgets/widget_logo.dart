import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetLogo extends StatelessWidget {
  final double widthImage;
  final double heightImage;
  final double heightDivider;
  final double widthText;
  final double heightText;
  final double fontSize;

  WidgetLogo({
    this.widthImage = 80,
    this.heightImage = 35,
    this.heightDivider = 40,
    this.widthText = 105,
    this.heightText = 50,
    this.fontSize = 20,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          AppImages.Logo,
          width: widthImage,
          height: heightImage,
          fit: BoxFit.fill,
        ),
        const SizedBox(width: 12),
        Container(
          height: heightDivider,
          width: 1.8,
          color: AppColors.primary,
        ),
        const SizedBox(width: 12),
        Container(
          width: widthText,
          height: heightText,
          child: Text(
            "Sông Tiên Corporation",
            style: AppStyles.DEFAULT_LARGE.copyWith(
              color: AppColors.primary,
              fontSize: fontSize,
            ),
          ),
        ),
      ],
    );
  }
}

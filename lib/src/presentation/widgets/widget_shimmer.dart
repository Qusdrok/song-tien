import 'package:flutter/material.dart';
import 'package:flutter_app_song_tien/src/configs/configs.dart';
import 'package:shimmer/shimmer.dart';

class WidgetShimmer extends StatelessWidget {
  final Widget child;
  final MaterialColor color;

  const WidgetShimmer({this.child, this.color});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: child,
      baseColor: color ?? AppColors.primaryDark.withOpacity(0.3),
      highlightColor: color ?? AppColors.primaryDark.withOpacity(0.1),
      enabled: true,
    );
  }
}

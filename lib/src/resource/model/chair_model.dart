import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_song_tien/src/configs/configs.dart';

enum ChairStatus {
  Empty,
  WaitingConfirmation,
  Confirmation,
  Other,
}

class ChairModel {
  ChairStatus status;
  String name;

  Color chairColor() {
    switch (status) {
      case ChairStatus.Empty:
        return AppColors.grey;
      case ChairStatus.WaitingConfirmation:
        return AppColors.red;
      case ChairStatus.Confirmation:
        return AppColors.primaryLight;
      case ChairStatus.Other:
        return AppColors.yellow;
    }
  }

  ChairModel({this.status, this.name});
}

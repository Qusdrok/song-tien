class CustomerModel {
  final int index;
  final String name;
  final String code;
  final bool isAccept;

  const CustomerModel({
    this.index,
    this.name,
    this.code,
    this.isAccept,
  });
}

class DistrictModel {
  DistrictModel({
    this.code,
    this.nameWithType,
    this.deliveryStatus,
    this.shipFeeBulky,
    this.shipFeeNotBulky,
  });

  String code;
  String nameWithType;
  List<int> deliveryStatus;
  int shipFeeBulky;
  int shipFeeNotBulky;

  static List<DistrictModel> listFromJson(dynamic json) {
    List<DistrictModel> data = [];
    if (json != null) {
      data = [];
      json.forEach((v) {
        data.add(DistrictModel.fromJson(v));
      });
    }
    return data;
  }

  factory DistrictModel.fromJson(Map<String, dynamic> json) => DistrictModel(
        code: json["code"],
        nameWithType: json["name_with_type"],
        deliveryStatus: List<int>.from(json["delivery_status"].map((x) => x)),
        shipFeeBulky: json["ship_fee_bulky"],
        shipFeeNotBulky: json["ship_fee_not_bulky"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "name_with_type": nameWithType,
        "delivery_status": List<dynamic>.from(deliveryStatus.map((x) => x)),
        "ship_fee_bulky": shipFeeBulky,
        "ship_fee_not_bulky": shipFeeNotBulky,
      };
}

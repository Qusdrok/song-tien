/// token : "0244d14157e0736cd916300f03981c19:d72d0f6b457eba1ef32f237b33cf720e3a329496cb25ce2600674e799554924c0c068f083be2a9495ef5ac6d5be9f5ec4b46f793991b3c2829156ca5f222fb3878719e0564d338d00e97d7740aaef900e14c6de3e9201f5382c5df76c399bacba668b9df305b887c3d9958a1b56a32275e15742b3c050c61fdb38f93a311af79aaf1617df51cbdcd0b398845ee298186"
/// msg : ""

class LoginResponse {
  String token;
  String msg;

  LoginResponse({
    this.token,
    this.msg});

  LoginResponse.fromJson(dynamic json) {
    token = json["token"];
    msg = json["msg"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["token"] = token;
    map["msg"] = msg;
    return map;
  }

}
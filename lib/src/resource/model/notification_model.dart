class NotificationModel {
  final String imageUrl;
  final String title;
  final String content;
  final DateTime time;

  const NotificationModel({this.imageUrl, this.title, this.content, this.time});
}

class ProvinceModel {
  ProvinceModel({
    this.code,
    this.name,
  });

  String code;
  String name;

  static List<ProvinceModel> listFromJson(dynamic json) {
    List<ProvinceModel> data = [];
    if (json != null) {
      data = [];
      json.forEach((v) {
        data.add(ProvinceModel.fromJson(v));
      });
    }
    return data;
  }

  factory ProvinceModel.fromJson(Map<String, dynamic> json) => ProvinceModel(
        code: json["code"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
      };
}

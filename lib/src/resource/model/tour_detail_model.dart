import '../../src.dart';

class TourDetailModel {
  List<TourModel> tours;
  UserModel user;
  TourModel tour;

  TourDetailModel({this.tours, this.user, this.tour});
}

class TourModel {
  final int star;
  final String name;
  final String type;
  final String imageUrl;
  final String code;

  const TourModel({this.star, this.name, this.type, this.imageUrl, this.code});
}

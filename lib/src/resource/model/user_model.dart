enum Privilege { Admin, Customer, Moderator }

class UserModel {
  String url;
  String name;
  String email;
  String phone;
  String uid;
  int type;
  Privilege privilege;

  UserModel({
    this.url,
    this.name,
    this.email,
    this.phone,
    this.type,
    this.uid,
  }) {
    privilege = getPrivilege();
  }

  Privilege getPrivilege() {
    switch (type) {
      case 0:
        print("Bạn là Admin");
        return Privilege.Admin;
      case 1:
        print("Bạn là Moderator");
        return Privilege.Moderator;
      case 2:
        print("Bạn là Customer");
        return Privilege.Customer;
    }
  }

  int getTabs() => (type + 4) - (type * 2);

  UserModel.fromJson(dynamic json) {
    if (json == null) return;
    url = json["url"];
    name = json["name"];
    email = json["email"];
    type = json["type"];
    uid = json["uid"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["url"] = url;
    map["name"] = name;
    map["email"] = email;
    map["type"] = type;
    map["uid"] = uid;
    return map;
  }
}

import 'package:dio/dio.dart';

import '../../src.dart';

enum LoginType { apple, facebook, google, phone }

class AuthRepository {
  AuthRepository._();

  static AuthRepository _instance;

  factory AuthRepository() {
    if (_instance == null) _instance = AuthRepository._();
    return _instance;
  }

  Future<NetworkState<LoginResponse>> resetPasswordByEmail({String email}) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Map<String, dynamic> params = {
        "email": email,
      };
      Response response = await AppClients().post(AppEndpoint.RESET_PASSWORD_BY_EMAIL, data: params);
      print(response.data);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => LoginResponse.fromJson(data),
        ),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<LoginResponse>> registerByPhone({
    String phone,
    String password,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Map<String, dynamic> params = {
        "pass": password,
        "phone": phone,
      };
      Response response = await AppClients().post(
        AppEndpoint.REGISTER_PHONE,
        data: params,
      );
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => LoginResponse.fromJson(data),
        ),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<UserModel>> login({
    String id,
    String accessToken,
    LoginType type,
    String email,
    String avatar,
    String phone,
    String fullName,
    String password,
  }) async {
    String api;
    Map<String, dynamic> params = {};
    switch (type) {
      case LoginType.apple:
        params.addAll({});
        api = AppEndpoint.LOGIN_FB;
        break;
      case LoginType.facebook:
        params.addAll({
          "facebookid": id,
          "fullname": fullName,
          "avatar": avatar,
          "facebooktoken": accessToken,
        });
        api = AppEndpoint.LOGIN_FB;
        break;
      case LoginType.google:
        params.addAll({
          "email": email ?? "",
          "fullname": fullName ?? "",
          "avatar": avatar,
          "googletoken": accessToken,
        });
        api = AppEndpoint.LOGIN_GOOGLE;
        break;
      case LoginType.phone:
        params.addAll({
          "email": email,
          "pass": password,
        });
        api = AppEndpoint.LOGIN_PHONE;
        break;
    }
    return await _sendRequestLogin(api, params);
  }

  Future<NetworkState<UserModel>> _sendRequestLogin(String api, Map<String, dynamic> params) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().post(api, data: params);
      NetworkState<LoginResponse> state = NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => LoginResponse.fromJson(data),
        ),
      );

      if (state.isSuccess && !state.isError && state.data.token != null) {
        await AppShared.setAccessToken(state.data.token);
      } else
        throw DioErrorType.CANCEL;
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProvinceModel>>> getProvinces() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_PROVINCE);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => ProvinceModel.listFromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<DistrictModel>>> getDistricts() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_DISTRICT);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => DistrictModel.listFromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }
}

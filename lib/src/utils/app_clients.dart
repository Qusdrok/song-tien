import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';

import '../configs/configs.dart';

class AppClients extends DioForNative {
  static const String GET = "GET";
  static const String POST = "POST";
  static const String PUT = "PUT";
  static const String DELETE = "DELETE";

  AppClients({String baseUrl = AppEndpoint.BASE_URL, BaseOptions options}) : super(options) {
    this.interceptors.add(
          InterceptorsWrapper(
            onRequest: _requestInterceptor,
            onResponse: _responseInterceptor,
            onError: _errorInterceptor,
          ),
        );
    this.options.baseUrl = baseUrl;
  }

  _requestInterceptor(RequestOptions options) async {
    options.connectTimeout = AppEndpoint.connectionTimeout;
    options.receiveTimeout = AppEndpoint.receiveTimeout;
    return options;
  }

  _responseInterceptor(Response response) {
    print("Response ${response.request.uri}: ${response.statusCode}\nData: ${response.data}");
  }

  _errorInterceptor(DioError dioError) {}
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_song_tien/main.dart';
import 'package:intl/intl.dart';

class AppUtils {
  AppUtils._();

  static final formatter = new NumberFormat("#,###");

  static String convertNumber(int amount) {
    return formatter.format(amount);
  }

  static void goToHome(BuildContext context) => Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => MyApp()),
        (route) => false,
      );

  static String convertDateTime2String(DateTime dateTime, {String format = 'dd/MM/yyyy - hh:mm'}) {
    if (dateTime == null) return "";
    return DateFormat(format).format(dateTime);
  }

  static DateTime convertString2DateTime(String dateTime, {String format = "yyyy-MM-ddTHH:mm:ss.SSSZ"}) {
    if (dateTime == null) return null;
    return DateFormat(format).parse(dateTime);
  }

  static String convertString2String(String dateTime,
      {String inputFormat = "yyyy-MM-ddTHH:mm:ss.SSSZ", String outputFormat = "yyyy-MM-dd"}) {
    if (dateTime == null) return "";
    final input = convertString2DateTime(dateTime, format: inputFormat);
    return convertDateTime2String(input, format: outputFormat);
  }

  static String minimum(int value) {
    if (value == null) return "00";
    return value < 10 ? "0$value" : "$value";
  }

  static String convertPhoneNumber(String phone, {String code = "+84"}) {
    return '$code${phone.substring(1)}';
  }

  static Map<String, dynamic> mapData(Map<String, dynamic> data) {
    return {"data": data ?? {}};
  }

  static Map<String, dynamic> parseData(Map<String, dynamic> data) {
    return data['data'] ?? {};
  }
}
